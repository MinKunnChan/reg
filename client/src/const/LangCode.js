export const LangCode = [
	{ name: "Japan", code: "ja" },
	{ name: "English", code: "en" },
	{ name: "China", code: "ch" },
	{ name: "Korea", code: "kr" }
];
