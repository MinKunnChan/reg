import React, { Component } from "react";
import Auxi from "../../hoc/Auxi";
import { Modal, message } from "antd";
import { Cropper } from "react-image-cropper";
import "./ImageUpload.css";
import FormData from "form-data";
import axios from "axios";
import { connect } from "react-redux";
import * as actions from "../../actions";

class ImageUpload extends Component {
	constructor(props) {
		super(props);
		this.state = {
			file: "",
			imagePreviewUrl: "",
			showCropView: false,
			imageUrl: "",
			visible: false,
			previewVisible: false
		};
	}

	handleImageChange(e) {
		e.preventDefault();

		let reader = new FileReader();
		let file = e.target.files[0];

		reader.onloadend = () => {
			this.setState({
				file: file,
				imagePreviewUrl: reader.result,
				showCropView: true,
				visible: true
			});
		};

		reader.readAsDataURL(file);
	}

	handleClick(e) {
		e.target.value = null;
	}

	handleOk = e => {
		const action = `/api/profileImage/${this.props.userId}`;
		const IMAGE_MAXIMUM_WIDTH = 250;

		//Resize Image
		var self = this;
		var tmpImg = new Image();
		tmpImg.src = this.refs.cropper.crop();
		tmpImg.onload = () => {
			if (
				tmpImg.width > IMAGE_MAXIMUM_WIDTH ||
				tmpImg.height > IMAGE_MAXIMUM_WIDTH
			) {
				var canvas = document.createElement("canvas");
				var maxlength =
					tmpImg.width < tmpImg.height ? tmpImg.height : tmpImg.width;
				var scale = IMAGE_MAXIMUM_WIDTH / maxlength;
				canvas.width = tmpImg.width * scale;
				canvas.height = tmpImg.height * scale;
				canvas
					.getContext("2d")
					.drawImage(
						tmpImg,
						0,
						0,
						tmpImg.width,
						tmpImg.height,
						0,
						0,
						canvas.width,
						canvas.height
					);
				tmpImg = new Image();
				tmpImg.src = canvas.toDataURL();
			}

			this.setState({
				visible: false
			});

			var file = dataURLtoFile(tmpImg.src, "hello");

			const formData = new FormData();

			formData.append("file", file);

			axios({
				method: "post",
				url: action,
				data: formData,
				config: { headers: { "Content-Type": "multipart/form-data" } }
			})
				.then(function(response) {
					self.props.fetchUser();
					//handle success
				})
				.catch(function(response) {
					message.error("upload failed.");
					//handle error
				});
		};
	};

	handleCancel = e => {
		this.setState({
			visible: false
		});
	};
	showPreviewModal = () => {
		this.setState({
			previewVisible: true
		});
	};
	handlePreviewOk = e => {
		this.setState({
			previewVisible: false
		});
	};

	render() {
		let { imagePreviewUrl, showCropView, imageUrl } = this.state;
		let $imagePreview = null;
		let previewImg = null;
		if (imageUrl) {
			$imagePreview = (
				<img className={this.props.imgClass} src={imageUrl} alt="previewImg" />
			);
			previewImg = (
				<img
					className={this.props.previewClass}
					src={imageUrl}
					alt="previewImg"
				/>
			);
		} else if (this.props.imageUrl) {
			$imagePreview = (
				<img
					className={this.props.imgClass}
					src={this.props.imageUrl}
					alt="previewImg"
				/>
			);
			previewImg = (
				<img
					className={this.props.previewClass}
					src={this.props.imageUrl}
					alt="previewImg"
				/>
			);
		} else {
			$imagePreview = (
				<div className="previewText">Please select an Image for Preview</div>
			);
			previewImg = (
				<div className="previewText">Please select an Image for Preview</div>
			);
		}
		const fromClass = this.props.isCollapsed
			? "g-margin-l5em"
			: "g-margin-l15em";

		return (
			<Auxi>
				<Modal
					title="Crop Your Image"
					visible={this.state.visible}
					maskClosable={false}
					onOk={this.handleOk}
					onCancel={this.handleCancel}>
					{showCropView ? (
						<div>
							<Cropper
								src={imagePreviewUrl}
								ratio={1 / 1}
								width={100}
								ref="cropper"
							/>
						</div>
					) : null}
				</Modal>

				<Modal
					title="Image Preview"
					width={300}
					visible={this.state.previewVisible}
					footer={null}
					onOk={this.handlePreviewOk}
					onCancel={this.handlePreviewOk}>
					{previewImg}
				</Modal>

				<div className={this.props.imgDivClass}>{$imagePreview}</div>

				<form className={fromClass}>
					<label className="fileContainer">
						<i className="fa fa-pencil-square-o" aria-hidden="true" />
						<input type="file" onChange={e => this.handleImageChange(e)}
							onClick={e => this.handleClick(e)} />
					</label>
					<label>
						<i
							className="fa fa-eye"
							style={{ cursor: "pointer" }}
							aria-hidden="true"
							onClick={this.showPreviewModal}
						/>
					</label>
				</form>
			</Auxi>
		);
	}
}

function dataURLtoFile(dataurl, filename) {
	var arr = dataurl.split(","),
	mime = arr[0].match(/:(.*?);/)[1],
	bstr = atob(arr[1]),
	n = bstr.length,
	u8arr = new Uint8Array(n);
	while (n--) {
		u8arr[n] = bstr.charCodeAt(n);
	}
	return new File([u8arr], filename, { type: mime });
}

export default connect(null, actions)(ImageUpload);
