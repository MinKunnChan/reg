import { FETCH_GUIDER, 
    CHANGE_TOURIST_TO_GUIDER,
    FETCH_USER_PROFILEINFO,
    UPDATE_PROFILE
} from '../actions/types';

export default function(state = {}, action) {
    switch(action.type) {
        case FETCH_GUIDER:
            return action.payload;
        case FETCH_USER_PROFILEINFO:
            return action.payload;
        case CHANGE_TOURIST_TO_GUIDER:
            return action.payload
        case UPDATE_PROFILE:
            return action.payload
        default:
            return state;
    }
}