import { FOLLOW, UNFOLLOW } from '../actions/types';

export default function(state = {}, action) {
    switch(action.type) {
        case FOLLOW:
			return action.payload;
		case UNFOLLOW:
			return action.payload;
        default:
            return state;
    }
}