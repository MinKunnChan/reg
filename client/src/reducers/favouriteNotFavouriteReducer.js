import { FAVOURITE, UNFAVOURITE } from "../actions/types";

export default function(state = {}, action) {
	switch (action.type) {
		case FAVOURITE:
			return action.payload;
		case UNFAVOURITE:
			return action.payload;
		default:
			return state;
	}
}
