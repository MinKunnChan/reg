import React, { Component } from "react";
import InfiniteScroll from "react-infinite-scroller";
import { List, Avatar } from "antd";
import { connect } from "react-redux";
import * as actions from "../../actions";
import { IMAGE_URL } from "../../const/ImageUrl";

class Following extends Component {
	componentDidMount() {
		this.props.fetchFollows({ touristId: this.props.user.userId });
	}

	onLoadMore = value => {
		console.log(`Load More ${value}`);
	};

	render() {
		const data = this.props.following;
		return (
			<div
				className="col s12 m6 l6 xl6"
				style={{ height: "455px", overflow: "auto" }}>
				<InfiniteScroll
					pageStart={0}
					loadMore={this.onLoadMore}
					hasMore={true}
					useWindow={false}>
					<h6>Following</h6>
					<List
						itemLayout="horizontal"
						dataSource={data}
						renderItem={item => (
							<div style={{ cursor: "pointer" }}>
								<List.Item>
									<List.Item.Meta
										avatar={<Avatar src={IMAGE_URL + item.profileImg} />}
										title={item.name}
									/>
									{/* <div style={{ marginLeft: 8 }}>
									<Button type="primary">Following</Button>
								</div> */}
								</List.Item>
							</div>
						)}
					/>
				</InfiniteScroll>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		user: state.auth,
		following: state.following
	};
}

export default connect(mapStateToProps, actions)(Following);
