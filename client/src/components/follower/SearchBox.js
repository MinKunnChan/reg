import React from "react";
import { Input } from "antd";
const Search = Input.Search;

const SearchBox = () => {
	return (
		<div style={{ width: "30%", marginLeft: 0 }} className="g-follower-search">
			<Search placeholder="..." onSearch={value => console.log(value)} />
		</div>
	);
};

export default SearchBox;
