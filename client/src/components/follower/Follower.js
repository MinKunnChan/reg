import React, { Component } from "react";
import InfiniteScroll from "react-infinite-scroller";
import { List, Avatar } from "antd";
import { connect } from "react-redux";
import * as actions from "../../actions";
import { IMAGE_URL } from "../../const/ImageUrl";

class Followers extends Component {
	componentDidMount() {
		this.props.fetchFollwers({ guiderId: this.props.user.userId });
	}

	onLoadMore = value => {
		// console.log(`Load More ${value}`);
	};

	render() {
		const data = this.props.followers;
		return (
			<div
				className="col s12 m6 l6 xl6"
				style={{ height: "455px", overflow: "auto" }}>
				<InfiniteScroll
					pageStart={0}
					loadMore={this.onLoadMore}
					hasMore={true}
					useWindow={false}>
					<h6>Follower</h6>
					<List
						itemLayout="horizontal"
						dataSource={data}
						renderItem={(item, index) => (
							<div style={{ cursor: "pointer" }} key={index}>
								<List.Item>
									<List.Item.Meta
										avatar={<Avatar src={IMAGE_URL + item.profileImg} />}
										title={item.name}
									/>
									{/* <div style={{ marginLeft: 8 }}>
												<Button type="primary">Follower</Button>
											</div> */}
								</List.Item>
							</div>
						)}
					/>
				</InfiniteScroll>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		user: state.auth,
		followers: state.followers
	};
}

export default connect(mapStateToProps, actions)(Followers);
