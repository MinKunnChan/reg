import React, { Component } from "react";
import Following from "./Following";
import Follower from "./Follower";

class FollowerWrapper extends Component {
	render() {
		return (
			<div>
				{/* <SearchBox /> */}
				<div className="row">
					<Follower />
					<Following />
				</div>
			</div>
		);
	}
}

export default FollowerWrapper;
