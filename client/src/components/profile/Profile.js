import _ from "lodash";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Rate, Button, Icon } from "antd";
import * as actions from "../../actions";

import TripList from "./trip/TripList";

import ImageList from "./gallery/ImageList";

import UpdateProfile from "./UpdateProfile";
import { Interesting } from "./Interesting-data";
import { Country } from "./Country-data";
import { GUIDER_ROLE_ID } from "../../const/index";
import Auxi from "../../hoc/Auxi";
class Profile extends Component {
	state = {
		isEditForm: false
	};
	componentDidMount() {
		let userId = this.props.user.userId;
		this.props.fetchGuider({
			guiderId: userId,
			langCode: "ja"
		});

		this.props.fetchTripList({
			userId: userId,
			langCode: "ja"
		});

		this.props.getGalleryAndFirstImage({
			userId: userId
		});
	}

	renderLangSkill() {
		return this.props.profile.langSkill.map((lang, index) => {
			return (
				<div key={index}>
					<div className="row g-margin-b1">
						<div className="col s12 m12 l3 xl3">
							<Button type="primary" className="g-width-full">
								{lang.langName}
							</Button>
						</div>
						<div className="col s12 m12 l9 xl9">
							<div className="ant-rate-text g-float-r g-space-r1">
								{lang.skillName}
							</div>
							<Rate
								character={<i className="fa fa-minus" aria-hidden="true" />}
								disabled
								value={lang.skillLevel}
								className="g-rate"
							/>
						</div>
					</div>
				</div>
			);
		});
	}

	renderCertifiedList() {
		return _.map(this.props.profile.certifiedList, certicate => {
			return (
				<div key={certicate} className="g-inline">
					<Button className="g-margin-r1 g-btn-default" size="small">
						{certicate}
					</Button>
				</div>
			);
		});
	}

	renderDetail = () => {
		const { roleId } = this.props.profile;
		const { profile } = this.props;

		const { greetingList } = this.props.profile;
		let greetingDetail = "";
		if (greetingList) {
			const greeting = greetingList.find(greet => greet.langCode === "ja");
			if (greeting) {
				greetingDetail = greeting.greetingDetail;
			}
		}

		let interesting = profile.interesting;
		let interArr = [];
		if (interesting) {
			interesting.map(ins => {
				return interArr.push(Interesting.find(obj => obj.code === ins));
			});
		}

		let country = {};

		if (profile.country) {
			country = _.find(Country, c => {
				return c.code === profile.country;
			});
		}

		const content =
			roleId === GUIDER_ROLE_ID ? (
				<Auxi>
					<div className="col s12 m7 l7 xl7">
						<strong className="g-space-r1">{profile.name}</strong>
						<Rate className="g-star-color" disabled value={profile.rating} />
						<strong>Reviews({profile.reviewCount})</strong>
						<p>{greetingDetail}</p>
					</div>
					<div className="col s12 m5 l5 xl5 verticalLine">
						{this.renderLangSkill()}
						<div className="g-margin-t1">{this.renderCertifiedList()}</div>
					</div>
				</Auxi>
			) : (
				<div className="col s12 m7 l7 xl7">
					<strong className="g-space-r1">{profile.name}</strong>
					<p>Country : {country.name}</p>
					{interArr.map(ins => {
						return <span key={ins.code}> {ins.name}</span>;
					})}
				</div>
			);

		return (
			<div className="g-detail shadow">
				<div className="container" style={{ width: "90%" }}>
					<div className="row g-padding">
						{content}
						<div className="col s12 m5 l5 xl5 g-margin-t15">
							<Button onClick={this.onEdit} size="small">
								Edit
							</Button>
						</div>
					</div>
				</div>
			</div>
		);
	};
	onEdit = () => {
		this.setState({ isEditForm: true });
	};

	handleCancel = () => {
		this.setState({ isEditForm: false });
	};

	render() {
		const { roleId } = this.props.profile;
		const isOwner = roleId === GUIDER_ROLE_ID ? true : false;
		return (
			<div>
				<h5>
					<Icon type="retweet" className="g-color" />
					{roleId === GUIDER_ROLE_ID ? "Guider Profile" : "Tourist Profile"}
				</h5>
				{this.state.isEditForm ? (
					<UpdateProfile
						isOwner={isOwner}
						updateRole={false}
						onCancelTrip={this.handleCancel}
					/>
				) : (
					this.renderDetail()
				)}
				{roleId === GUIDER_ROLE_ID ? (
					<div>
						<TripList isOwner={true} onEditTrip={this.props.onEditTrip} />
						<ImageList />
					</div>
				) : (
					<div style={{ height: "190px" }} />
				)}

				{/* <ReviewList /> */}
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		profile: state.profile,
		user: state.auth
	};
}

export default connect(mapStateToProps, actions)(Profile);
