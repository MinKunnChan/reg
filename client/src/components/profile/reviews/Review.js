import React from "react";
import { Rate } from 'antd';

import img1 from '../../../image/user2.png';

const Review = () => {

  return (
      <div>
        <div className="row">   
            <div className="col s2">
                <img className="responsive-img circle g-img" src={img1} alt="avator"/>
            </div>
            <div className="col s10">
               <strong>Yamada </strong><span className="g-space-2"></span>
                   <Rate className="g-star-color" allowHalf defaultValue={3}/>
                 <p><strong className="g-color">The standard Lorem Ipsum </strong></p>
                 <p>
                 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                 </p>
            </div>
        </div>
        <hr />
      </div>
  );
};

export default Review;
