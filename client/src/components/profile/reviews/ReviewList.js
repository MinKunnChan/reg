import React from 'react';
import { Icon  } from 'antd';

import Review from "./Review";

const ReviewList = props => {
  return (
      <div>
            <h5>
                <Icon type="retweet"  className="g-color g-review-header" />
                Review List
            </h5>
            <hr />
            <ul>
                <li><Review /></li>
                <li><Review /></li>
                <li><Review /></li>
                <li><Review /></li>
                <li><Review /></li>
            </ul>
       </div>
      
  );
};
export default ReviewList;