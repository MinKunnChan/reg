import React, { Component } from "react";
import {
	Form,
	Input,
	Button,
	Select,
	message,
	Tabs,
	Radio,
	Modal,
	Checkbox
} from "antd";
import { connect } from "react-redux";
import * as actions from "../../actions";

import CertificateForm from "./CertificateForm";
import CreateLanguage from "./CreateLanguage";

import { Country } from "./Country-data";
import { Interesting } from "./Interesting-data";
import { LangCode } from "../../const/LangCode";
import { Link } from "react-router-dom";
import Auxi from "../../hoc/Auxi";

const RadioGroup = Radio.Group;
const TabPane = Tabs.TabPane;
const { TextArea } = Input;
const FormItem = Form.Item;
const Option = Select.Option;

const LangKey = {
	Japan: "ja",
	English: "en",
	China: "ch",
	Korea: "kr"
};

const interesting = [];
Interesting.forEach(i => {
	interesting.push(<Option key={i.code}>{i.name}</Option>);
});

const countries = [];
Country.forEach(c => {
	countries.push(<Option key={c.code}>{c.name}</Option>);
});

const CreateForm = Form.create()(props => {
	const {
		onCreate,
		onCreateLanguage,
		onDeleteLanguage,
		onCancel,
		form,
		isOwner,
		disabled,
		panes,
		addBtn,
		onTabChange,
		activeKey,
		onEdit,
		langSkill,
		changeToguider,
		isAgree,
		onCheckChange
	} = props;
	const { getFieldDecorator } = form;
	return (
		<Form onSubmit={onCreate}>
			<FormItem label="Name" className="row">
				{getFieldDecorator("name", {
					rules: [{ required: true, message: "Please type Name!" }]
				})(<Input />)}
			</FormItem>
			{isOwner ? (
				<FormItem label="E-mail" className="row">
					{getFieldDecorator("email", {
						rules: [
							{
								type: "email",
								message: "The input is not valid E-mail!"
							},
							{
								required: true,
								message: "Please input your E-mail!"
							}
						]
					})(<Input disabled={disabled} />)}
				</FormItem>
			) : null}

			<FormItem label="Interesting" className="row g-margin-b8">
				{getFieldDecorator("interesting", {
					rules: [{ required: true, message: "Please select your Interest!" }]
				})(<Select mode="tags">{interesting}</Select>)}
			</FormItem>

			<FormItem label="Country" className="row g-margin-b8">
				{getFieldDecorator("country", {
					rules: [{ required: true, message: "Please select your Country!" }]
				})(
					<Select
						showSearch
						optionFilterProp="country"
						filterOption={(input, option) =>
							option.props.children
								.toLowerCase()
								.indexOf(input.toLowerCase()) >= 0
						}>
						{countries}
					</Select>
				)}
			</FormItem>
			{isOwner ? (
				<Auxi>
					<div className="g-margin-t13">
						<Tabs
							tabBarExtraContent={addBtn}
							hideAdd
							onChange={onTabChange}
							activeKey={activeKey}
							type="editable-card"
							onEdit={onEdit}>
							{panes.map(pane => (
								<TabPane tab={pane.title} key={pane.key}>
									{pane.content}
								</TabPane>
							))}
						</Tabs>
					</div>

					<CreateLanguage
						langSkill={langSkill}
						onCreateLanguage={onCreateLanguage}
						onDeleteLanguage={onDeleteLanguage}
					/>
				</Auxi>
			) : null}
			{changeToguider ? (
				<FormItem>
					{getFieldDecorator("agreement", {
						valuePropName: "checked"
					})(
						<Checkbox onChange={onCheckChange}>
							I have read the
							<Link target="_blank" to={"/aggrement"}>
								{" "}
								agreement
							</Link>
						</Checkbox>
					)}
				</FormItem>
			) : null}

			<div classes="g-margin-t1">
				<Button
					type="primary"
					htmlType="submit"
					disabled={isAgree ? false : true}
					className="g-btn-submit right">
					Update
				</Button>
				<Button onClick={onCancel} className="right g-margin-r8">
					Cancel
				</Button>
			</div>
		</Form>
	);
});

class GreetingDetail extends Component {
	state = { value: "" };

	componentWillReceiveProps(nextProps) {
		this.setState({ value: nextProps.greetingDetail });
	}

	onChange = e => {
		e.preventDefault();
		this.setState({ value: e.target.value });
		this.props.onChange(e);
	};
	render() {
		const detail = this.props.greetingDetail;
		let value = this.state.value ? this.state.value : detail;
		return (
			<Auxi>
				<span className="g-span-label">Greeting Detail</span>
				<TextArea value={value} onChange={this.onChange} rows={4} />
			</Auxi>
		);
	}
}

class UpdateProfile extends Component {
	state = {
		fileList: [],
		imageList: [],
		langSkill: [],
		greeting: {},
		certifiedList: [],
		greetingList: [{ langCode: "ja" }],
		greetingDetail: "",
		value: "ja",
		title: "Japan",
		isLangAlready: false,
		visibleModal: false,
		activeKey: "ja",
		panes: [],
		isAgree: false
	};

	componentDidMount() {
		this.fetchUserProfileInfo(
			this.props.profile.userId,
			this.props.profile.roleId,
			"ja"
		);
		this.setState({
			panes: [
				{ title: "Japan", content: this.createTab({}), key: LangKey.Japan }
			]
		});
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.profile !== nextProps.profile) {
			this.form.setFieldsValue({
				name: nextProps.profile.name,
				email: nextProps.profile.email,
				interesting: nextProps.profile.interesting,
				country: nextProps.profile.country
			});
			const greetingList = nextProps.profile.greetingList;
			let panes = [];
			if (greetingList) {
				greetingList.map(greet => {
					const key = greet.langCode;
					const title = LangCode.find(lang => {
						return lang.code === key;
					});
					return panes.push({
						title: title.name,
						content: this.createTab(greet),
						key: key
					});
				});
			}
			const updatedPanes = panes.length > 0 ? panes : this.state.panes;
			this.setState({
				langSkill: nextProps.profile.langSkill
					? nextProps.profile.langSkill
					: [],
				greetingList: nextProps.profile.greetingList
					? nextProps.profile.greetingList
					: [],
				panes: updatedPanes
			});
		}
	}
	onGreetingChange = e => {
		const greetingList = [...this.state.greetingList];
		let greetingIndex = greetingList.findIndex(greet => {
			return greet.langCode === this.state.activeKey;
		});
		let greeting = null;
		if (greetingIndex > -1) {
			//found object
			greeting = greetingList[greetingIndex];
			greeting["greetingDetail"] = e.target.value;
		} else {
			//not found and create new one
			greeting = { langCode: this.state.activeKey };
			greeting["greetingDetail"] = e.target.value;
			greetingIndex = greetingList.length;
		}
		greetingList[greetingIndex] = greeting;
		this.setState({ greetingList, greetingDetail: e.target.value });
	};
	createTab = greeting => {
		const greetingDetail = greeting.greetingDetail;
		return (
			<Auxi>
				<GreetingDetail
					greetingDetail={greetingDetail}
					onChange={this.onGreetingChange}
				/>

				<CertificateForm
					certifiedList={greeting.certifiedList}
					onCreateCertificate={this.handleCreateCertificate}
					onDeleteCertificate={this.handleDeleteCertificate}
				/>
			</Auxi>
		);
	};

	saveFormRef = form => {
		this.form = form;
	};
	onTabChange = activeKey => {
		this.setState({ activeKey });
	};

	onEdit = (targetKey, action) => {
		const { panes } = this.state;
		const greetingList = [...this.state.greetingList];
		// we not allow to delete all language panes
		if (panes.length === 1) return;
		const paneIndex = panes.findIndex(pane => {
			return pane.key === targetKey;
		});
		const greetingIndex = greetingList.findIndex(greet => {
			return greet.langCode === targetKey;
		});
		panes.splice(paneIndex, 1);
		greetingList.splice(greetingIndex, 1);
		const index = panes.length - 1;
		const activeKey = panes[index].key;
		this.setState({ panes, activeKey, greetingList });
	};

	onOk = () => {
		const { panes, value } = this.state;
		const paneIndex = panes.findIndex(pane => {
			return pane.key === value;
		});
		//if the language pane is already selected, we ignore it
		if (paneIndex >= 0) {
			this.setState({ isLangAlready: true });
			return;
		}
		const activeKey = value;

		panes.push({
			title: this.state.title,
			content: this.createTab({}),
			key: activeKey
		});

		this.setState({
			visibleModal: false,
			panes,
			activeKey,
			isLangAlready: false
		});
	};

	showModal = () => {
		this.setState({
			visibleModal: true
		});
	};

	handleCloseModal = () => {
		this.setState({
			visibleModal: false
		});
	};
	onSelectLanguage = e => {
		const val = e.target.value;
		const title = LangCode.find(lang => {
			return lang.code === val;
		});
		this.setState({
			value: val,
			title: title.name,
			isLangAlready: false
		});
	};

	handleCreate = e => {
		e.preventDefault();

		const form = this.form;
		form.validateFields((err, values) => {
			if (err) {
				return;
			}

			values.userId = this.props.profile.userId;

			values.langCode = "ja";
			values.greetingList = this.state.greetingList;

			let langSkill = Object.assign([], this.state.langSkill);

			langSkill = langSkill.filter(function(langSkill) {
				return delete langSkill._id;
			});

			values.langSkill = langSkill;

			console.log("Values ", values);

			if (this.props.changeToguider) {
				this.props.changeTouristToGuider(values).then(res => {
					this.props.updateContent();
				});
			} else {
				values.roleId = this.props.profile.roleId;
				this.props.updateProfile(values);
				this.fetchUserProfileInfo(
					this.props.profile.userId,
					this.props.profile.roleId,
					"ja"
				);
				this.props.fetchUser();
				this.props.onCancelTrip();
			}
			form.resetFields();
		});
	};

	fetchUserProfileInfo(userId, roleId, langCode) {
		this.props.fetchUserProfileInfo({ userId, roleId, langCode });
	}

	handleCreateLanguage = langSkill => {
		this.setState({ langSkill });
	};
	handleDeleteLanguage = langSkill => {
		let array = [...this.state.langSkill];
		let index = array.indexOf(langSkill);
		array.splice(index, 1);
		this.setState({ langSkill: array });
	};

	handleCreateCertificate = certifiedList => {
		const greetingList = [...this.state.greetingList];
		let greetingIndex = greetingList.findIndex(greet => {
			return greet.langCode === this.state.activeKey;
		});
		let greeting = null;
		if (greetingIndex > -1) {
			//found object
			greeting = greetingList[greetingIndex];
			greeting["certifiedList"] = certifiedList;
		} else {
			//not found and create new one
			greeting = { langCode: this.state.activeKey };
			greeting["certifiedList"] = certifiedList;
			greetingIndex = greetingList.length;
		}
		greetingList[greetingIndex] = greeting;
		this.setState({ greetingList });
	};

	handleDeleteCertificate = (e, index) => {
		e.preventDefault();
		const greetingList = [...this.state.greetingList];
		let greetingIndex = greetingList.findIndex(greet => {
			return greet.langCode === this.state.activeKey;
		});
		const greeting = greetingList[greetingIndex];
		let certifiedList = greeting.certifiedList;
		certifiedList.splice(index, 1);
		greeting["certifiedList"] = certifiedList;
		greetingList[greetingIndex] = greeting;
		this.setState({ greetingList });
	};

	handleChange = info => {
		let fileList = info.fileList;

		fileList = fileList.map(file => {
			if (file.response) {
				this.setState({ imageList: file.response });
				message.success("upload successfully.");
				// Component will show file.url as link
				file.url = file.response.url;
			}
			return file;
		});

		this.setState({ fileList });
	};

	handleCheckBoxChange = () => {
		const isAgree = !this.state.isAgree;
		this.setState({ isAgree });
	};

	render() {
		const panes = [...this.state.panes];
		const operations = (
			<Button onClick={this.showModal} shape="circle" icon="plus" />
		);
		let disabled = this.props.profile.email ? true : false;
		const header = this.props.changeToguider ? (
			<h6>
				In order to create a new trip, you need to be a guider first. Please
				fill out the necessary information.
			</h6>
		) : (
			<h6>Update Profile</h6>
		);
		return (
			<div>
				{header}
				<div className="row">
					<div className="col s10">
						<CreateForm
							langSkill={this.state.langSkill}
							ref={this.saveFormRef}
							isOwner={this.props.isOwner}
							onCancel={this.props.onCancelTrip}
							onCreate={this.handleCreate}
							onCreateLanguage={this.handleCreateLanguage}
							onDeleteLanguage={this.handleDeleteLanguage}
							disabled={disabled}
							panes={panes}
							addBtn={operations}
							onTabChange={this.onTabChange}
							activeKey={this.state.activeKey}
							onEdit={this.onEdit}
							isAgree={this.props.changeToguider ? this.state.isAgree : true}
							onCheckChange={this.handleCheckBoxChange}
							changeToguider={this.props.changeToguider}
						/>
					</div>
				</div>

				<Modal
					title="Add Language"
					visible={this.state.visibleModal}
					onOk={this.onOk}
					onCancel={this.handleCloseModal}>
					<RadioGroup onChange={this.onSelectLanguage} value={this.state.value}>
						<Radio value={LangKey.Japan} defaultChecked>
							Japan
						</Radio>
						<Radio value={LangKey.English}>English</Radio>
						<Radio value={LangKey.China}>China</Radio>
						<Radio value={LangKey.Korea}>Korea</Radio>
					</RadioGroup>
					{this.state.isLangAlready ? (
						<p style={{ color: "red" }}>This Language already selected</p>
					) : null}
				</Modal>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		user: state.auth,
		profile: state.profile
	};
}

export default connect(mapStateToProps, actions)(UpdateProfile);
