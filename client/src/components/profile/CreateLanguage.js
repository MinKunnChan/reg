import React, { Component } from "react";
import { Input, Button, Select, Icon, Form } from "antd";
import uuid from "uuid";
const FormItem = Form.Item;

const Option = Select.Option;
const LANG_LEVEL = [
	{
		key: 1,
		value: "Beginner"
	},
	{
		key: 2,
		value: "Intermediate"
	},
	{
		key: 3,
		value: "Advanced"
	},
	{
		key: 4,
		value: "Business Level"
	}
];

const LanguageForm = Form.create({
	mapPropsToFields(props) {
		return {
			langName: Form.createFormField({
				value: props.lang.langName
			}),
			skillLevel: Form.createFormField({
				value: props.lang.skillLevel
			})
		};
	}
})(props => {
	const { getFieldDecorator } = props.form;

	return (
		<div>
			<FormItem label="Language Name">
				{getFieldDecorator("langName", {
					rules: [{ required: true, message: "Please input Language Name!" }]
				})(<Input placeholder="Name..." />)}
			</FormItem>
			<FormItem label="Level">
				{getFieldDecorator("skillLevel", {
					rules: [
						{ required: true, message: "Please select your language level!" }
					]
				})(
					<Select placeholder="select a level">
						{LANG_LEVEL.map(lang => {
							return (
								<Option value={lang.key} key={lang.key}>
									{lang.value}
								</Option>
							);
						})}
					</Select>
				)}
			</FormItem>

			<FormItem>
				<Button
					type="primary"
					htmlType="submit"
					onClick={props.onSubmit}
					className="g-btn-submit right g-margin-t8">
					Save
				</Button>
				<Button
					onClick={props.onCancel}
					className="right g-margin-r8 g-margin-t8">
					Cancel
				</Button>
			</FormItem>
		</div>
	);
});
class CreateLanguage extends Component {
	state = {
		inputVisible: false,
		langSkill: {}
	};

	onAdd = () => {
		this.setState({ inputVisible: true });
	};

	handleSubmit = e => {
		e.preventDefault();

		this.form.validateFields((err, values) => {
			if (!err) {
				const langIndex = this.props.langSkill.findIndex(lang => {
					return lang._id === this.state.langSkill._id;
				});

				const level = LANG_LEVEL.find(lev => {
					return lev.key === values.skillLevel;
				});

				if (langIndex !== -1) {
					//update
					const lang = {
						...this.props.langSkill[langIndex]
					};

					lang.skillName = level.value;
					lang.skillLevel = level.key;
					lang.langName = values.langName;
					lang.langCode = "en";

					const langSkill = [...this.props.langSkill];
					langSkill[langIndex] = lang;

					this.setState({
						langSkill: {},
						inputVisible: false
					});
					this.form.resetFields();
					this.props.onCreateLanguage(langSkill);
				} else {
					//insert
					let langSkill = this.props.langSkill;
					const id = uuid.v4();
					let lanObj = {
						_id: id,
						skillName: level.value,
						skillLevel: level.key,
						langName: values.langName,
						langCode: "en"
					};
					if (lanObj) {
						langSkill = [...langSkill, lanObj];
					}
					this.form.resetFields();
					this.setState({
						langSkill: {},
						inputVisible: false
					});
					this.props.onCreateLanguage(langSkill);
				}
			}
		});
	};

	onCancel = () => {
		this.form.resetFields();
		this.setState({
			inputVisible: false
		});
	};

	saveFormRef = form => {
		this.form = form;
	};

	handleEdit = (event, langSkill) => {
		this.setState({
			inputVisible: true,
			langSkill: langSkill
		});
	};

	renderForm = () => {
		return (
			<div className="g-width30">
				<LanguageForm
					lang={this.state.langSkill}
					ref={this.saveFormRef}
					onCancel={this.onCancel}
					onSubmit={this.handleSubmit}
				/>
			</div>
		);
	};

	render() {
		const { inputVisible } = this.state;
		const langSkill = this.props.langSkill;

		let langSkillContent = null;
		if (langSkill) {
			langSkillContent = langSkill.map((lang, index) => {
				return (
					<div key={index} className="g-width300 g-margin-b0">
						<div className="g-lang-block">{lang.langName}</div>
						<span className="g-lang-level">
							{lang.skillName}
							<i
								className="fa fa-cog fa-lg g-level-edit"
								onClick={event => this.handleEdit(event, lang)}
								aria-hidden="true"
							/>
							<i
								onClick={() => this.props.onDeleteLanguage(lang)}
								className="fa fa-trash fa-lg g-level-delete"
							/>
						</span>
					</div>
				);
			});
		}
		return (
			<div>
				<span className="g-span-label">Languages</span>
				{langSkillContent}
				{inputVisible && this.renderForm()}
				{!inputVisible && (
					<Button onClick={this.onAdd} className="g-btnAdd">
						<Icon type="plus-circle-o" />Add Language
					</Button>
				)}
			</div>
		);
	}
}

export default CreateLanguage;
