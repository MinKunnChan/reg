import _ from "lodash";
import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../../../actions";
import { Icon } from "antd";
import ImageView from "./ImageView";
import { IMAGE_URL } from "../../../const/ImageUrl";

class ImageList extends Component {
	state = {
		index: 0
	};
	activeBorder = (e, index) => {
		this.setState({ index });
	};
	renderGalleryList() {
		return _.map(this.props.gallery, (image, index) => {
			return (
				<img
					key={index}
					src={IMAGE_URL + image}
					alt="imagelist"
					onClick={e => {
						this.props.selectImage(image);
						this.activeBorder(e, index);
					}}
					className={
						this.state.index === index
							? "g-gallery-list-active"
							: "g-gallery-list"
					}
				/>
			);
		});
	}

	render() {
		const content =
			this.props.gallery.length > 0 ? (
				<div>
					<h5 className="g-margin">
						<Icon type="retweet" className="g-color" />Guider Gallary
					</h5>
					<div className="row">
						<ImageView />
						<div className="col s12 m4 l4 xl4 g-margin-gallery">
							{this.renderGalleryList()}
						</div>
					</div>
				</div>
			) : null;
		return content;
	}
}

function mapStateToProps(state) {
	return {
		gallery: state.gallery.gallery
	};
}

export default connect(mapStateToProps, actions)(ImageList);
