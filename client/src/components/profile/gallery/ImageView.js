import React, { Component } from "react";
import { connect } from "react-redux";
import { IMAGE_URL } from "../../../const/ImageUrl";

class ImageView extends Component {
	render() {
		return (
			<div className="col s12 m8 l8 xl8">
				<div className="g-galleryview">
					<img
						src={IMAGE_URL + this.props.image}
						alt="avator"
						className="g-img-view"
					/>
				</div>
				<div className="details" />
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		image: state.gallery.image
	};
}

export default connect(mapStateToProps)(ImageView);
