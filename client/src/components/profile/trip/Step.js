import React from "react";
import { IMAGE_URL } from "../../../const/ImageUrl";

const Step = ({ step, index, onMobile }) => {
	const content = onMobile ? (
		<div className="g-margin-b0 row">
			<div className="col s12 m6 l6 xl6" style={{ padding: "0rem" }}>
				<img
					alt="example"
					className="g-img-view"
					src={IMAGE_URL + step.stepImgList[0]}
				/>
			</div>
			<div className="col s12 m6 l6 xl6 g-step-div">
				<div className="col s4">
					<h2 className="g-step-list">0{index}</h2>
				</div>
				<div className="col s8 g-trip-content">
					<h5 className="g-margin-top-bottom3">{step.title}</h5>
					<p className="g-pstyle">{step.description}</p>
				</div>
			</div>
		</div>
	) : index % 2 !== 0 ? (
		<div className="g-margin-b0 row">
			<div className="col s12 m6 l6 xl6" style={{ padding: "0rem" }}>
				<img
					alt="example"
					className="g-img-view"
					src={IMAGE_URL + step.stepImgList[0]}
				/>
			</div>
			<div className="col s12 m6 l6 xl6 g-step-div">
				<div className="col s4">
					<h2 className="g-step-list">0{index}</h2>
				</div>
				<div className="col s8 g-trip-content">
					<h5 className="g-margin-top-bottom3">{step.title}</h5>
					<p className="g-pstyle">{step.description}</p>
				</div>
			</div>
		</div>
	) : (
		<div className="g-margin-b0 row">
			<div className="col s12 m6 l6 xl6 g-step-div">
				<div className="col s4">
					<h2 className="g-step-list">0{index}</h2>
				</div>
				<div className="col s8 g-trip-content">
					<h5 className="g-margin-top-bottom3">{step.title}</h5>
					<p className="g-pstyle">{step.description}</p>
				</div>
			</div>
			<div className="col s12 m6 l6 xl6" style={{ padding: "0rem" }}>
				<img
					alt="example"
					className="g-img-view"
					src={IMAGE_URL + step.stepImgList[0]}
				/>
			</div>
		</div>
	);
	return content;
};

export default Step;
