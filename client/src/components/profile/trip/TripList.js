import _ from "lodash";
import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../../../actions";
import { Icon } from "antd";
import Trip from "./Trip";

class TripList extends Component {
	renderTripList() {
		return _.map(this.props.tripList, trip => {
			return (
				<Trip
					isOwner={this.props.isOwner}
					key={trip._id}
					trip={trip}
					onEditTrip={this.props.onEditTrip}
					onDelete={this.handleDelete}
				/>
			);
		});
	}

	handleDelete = trip => {
		this.props.deleteTrip({ tripId: trip._id }).then(() => {
			this.props.fetchTripList({
				userId: this.props.user.userId,
				langCode: "ja"
			});
			this.props.getGalleryAndFirstImage({
				userId: this.props.user.userId
			});
		});
	};

	render() {
		const content =
			this.props.tripList.length > 0 ? (
				<div>
					<h5>
						<Icon type="retweet" className="g-color" />
						Trip List
					</h5>
					<div className="g-border">{this.renderTripList()}</div>
				</div>
			) : null;
		return content;
	}
}

function mapStateToProps(state) {
	return {
		tripList: state.trips.all,
		user: state.auth
	};
}

export default connect(mapStateToProps, actions)(TripList);
