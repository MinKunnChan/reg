import React from "react";
import { Button, Tag } from "antd";
const Steps = ({ step, onDelete, onEdit }) => {
	return (
		<div className="col s12 m6 l6 xl6">
			<h5 className="g-font-color">
				<i className="fa fa-compass" aria-hidden="true" /> {step.title}
			</h5>
			<p>{step.description}</p>
			<div className="g-price-box">
				<div className="row g-margin-b1">
					<div className="col s6">
						<div>
							<span>Location Name : </span>
							<span>{step.locationName} </span>
						</div>
						<div>
							<span>Duration : </span>
							<span>
								{step.duration} {step.durationUnit}
							</span>
						</div>
					</div>
					<div className="col s6">
						<div>Additional Expense</div>
						{step.additionalInfo.map(tag => {
							return (
								<Tag
									className="g-btnAdd"
									key={tag.name}
									color="blue"
									closable={false}>
									{tag.name} : {tag.value}
								</Tag>
							);
						})}
					</div>
				</div>
			</div>
			<div className="g-margin-t8">
				<Button
					onClick={() => onDelete(step)}
					type="danger"
					size="small"
					className="right">
					Delete
				</Button>
				<Button
					onClick={() => onEdit(step)}
					className="right g-margin-r8"
					size="small">
					Edit
				</Button>
			</div>
		</div>
	);
};

export default Steps;
