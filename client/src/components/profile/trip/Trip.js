import React from "react";
import { Rate, Button } from "antd";
import { Link } from "react-router-dom";
import { IMAGE_URL } from "../../../const/ImageUrl";

const Trip = ({ trip, isOwner, onDelete, onEditTrip }) => {
	const editDeleteButton = isOwner ? (
		<span className="g-margin-l15">
			<Button
				size="small"
				className="g-margin-r8"
				onClick={() => onEditTrip(trip._id)}>
				Edit
			</Button>
			<Button onClick={() => onDelete(trip)} type="danger" size="small">
				Delete
			</Button>
		</span>
	) : (
		""
	);

	return (
		<div>
			<div className="row g-margin-b1">
				<div className="col s12 m6 l6 xl6">
					<div className="g-img-box">
						<img
							className="g-trip-img"
							src={IMAGE_URL + trip.imageList[0]}
							alt="beauty"
						/>
					</div>
				</div>
				<div className="col s12 m6 l6 xl6">
					<div className="g-img-box">
						<h5>{trip.title}</h5>
						<p>{trip.description}</p>
						<div className="g-price-box">
							<div className="row g-margin-b1">
								<div className="col s12 m3 l3 xl3">
									<strong>Rating</strong>
								</div>
								<div className="col s12 m9 l9 xl9">
									<Rate className="g-star-color" disabled value={trip.rating} />
								</div>
							</div>
							<div className="row g-margin-b1">
								<div className="col s12 m3 l3 xl3">
									<strong>Duration</strong>
								</div>
								<div className="col  s12 m3 l3 xl3">
									{trip.duration} {trip.durationUnit}
								</div>
								<div className="col  s12 m3 l3 xl3">
									<strong>Price </strong>
								</div>
								<div className="col  s12 m3 l3 xl3">{trip.price}</div>
							</div>
							<div className="row g-margin-b1">
								<div className="col  s12 m3 l3 xl3">
									<strong>Minimum People</strong>
								</div>
								<div className="col s12 m9 l9 xl9">{trip.minimumPeople}</div>
							</div>
						</div>
					</div>
					{editDeleteButton}
					<Link
						className="ant-btn g-btn-submit g-float-right ant-btn-primary g-btn-right"
						to={"/tripDetail/" + trip._id}>
						View More
					</Link>
				</div>
			</div>
			<hr className="g-hr" />
		</div>
	);
};

export default Trip;
