import React from "react";
import { Input, Button, Form } from "antd";
import AdditionalInfo from "./AdditionalInfo";

const FormItem = Form.Item;
const { TextArea } = Input;

const CreateStepForm = Form.create({
	onValuesChange(props, changedFields) {
		props.onFormValueChange(changedFields, props.step);
	},
	mapPropsToFields(props) {
		return {
			title: Form.createFormField({
				value: props.step.title
			}),
			description: Form.createFormField({
				value: props.step.description
			}),
			locationName: Form.createFormField({
				value: props.step.locationName
			}),
			duration: Form.createFormField({
				value: props.step.duration
			}),
			durationUnit: Form.createFormField({
				value: props.step.durationUnit
			})
			// additionalInfo: Form.createFormField({
			// 	value: props.step.additionalInfo
			// })
		};
	}
})(props => {
	const { form, onSaveStep, handleAdditionalInfo, onCancelEdit, step } = props;
	const { getFieldDecorator } = form;
	return (
		<Form onSubmit={onSaveStep}>
			<FormItem label="Title">
				{getFieldDecorator("title", {
					rules: [{ required: true, message: "Please input Title!" }]
				})(<Input />)}
			</FormItem>
			<FormItem label="Description">
				{getFieldDecorator("description")(<TextArea rows={3} />)}
			</FormItem>
			<FormItem label="Location Name">
				{getFieldDecorator("locationName", {
					rules: [{ required: true, message: "Please input Location Name!" }]
				})(<Input />)}
			</FormItem>
			<div className="row g-margin-b0">
				<div className="col s7" style={{ padding: 0 }}>
					<FormItem label="Duration">
						{getFieldDecorator("duration", {
							rules: [{ required: true, message: "Please input Duration!" }]
						})(<Input type="number" />)}
					</FormItem>
				</div>
				<div style={{ paddingRight: 0 }} className="col s5">
					<FormItem label="Duration Unit">
						{getFieldDecorator("durationUnit", {
							rules: [
								{ required: true, message: "Please input Duration Unit!" }
							]
						})(<Input />)}
					</FormItem>
				</div>
			</div>
			<AdditionalInfo
				additionalInfo={step.additionalInfo}
				onSaveAdditionalInfo={handleAdditionalInfo}
			/>

			<FormItem style={{ marginBottom: "0px", marginTop: "8px" }}>
				<Button type="primary" htmlType="submit" className="g-btn-submit right">
					Save
				</Button>
				<Button onClick={onCancelEdit} className="right g-margin-r8">
					Cancel
				</Button>
			</FormItem>
		</Form>
	);
});

export default CreateStepForm;
