import React, { Component } from "react";
import { Button, Icon, Tag, Input } from "antd";

class AdditionalInfo extends Component {
	constructor(props) {
		super(props);

		this.state = {
			tags: [],
			inputVisible: false,
			inputValue: "",
			inputName: ""
		};
	}

	componentWillMount() {
		const { additionalInfo } = this.props;
		if (additionalInfo) {
			this.setState({ tags: additionalInfo });
		}
	}

	handleClose = removedTag => {
		let index = this.state.tags.indexOf(removedTag);

		if (index > -1) {
			this.state.tags.splice(index, 1);
		}
	};

	showInput = () => {
		this.setState({ inputVisible: true });
	};

	handleValueChange = e => {
		this.setState({ inputValue: e.target.value });
	};
	handleNameChange = e => {
		this.setState({ inputName: e.target.value });
	};

	handleInputConfirm = (e) => {
		e.preventDefault();
		const state = this.state;
		const inputName = state.inputName;
		const inputValue = state.inputValue;
		let tags = state.tags;
		if (inputName) {
			let val = { name: inputName, value: inputValue };
			tags = [...tags, val];
		}
		this.setState({
			tags,
			inputVisible: false,
			inputValue: "",
			inputName: ""
		});
		this.props.onSaveAdditionalInfo(tags);
	};

	render() {
		const { tags, inputVisible } = this.state;
		return (
			<div>
				<div className="g-form-required">Additionl Expense</div>

				{tags.map((tag, index) => {
					return (
						<Tag
							key={index}
							className="g-btnAdd"
							color="blue"
							closable={true}
							afterClose={() => this.handleClose(tag)}>
							<span>
								{tag.name} : {tag.value}
							</span>
						</Tag>
					);
				})}

				{inputVisible && (
					<div>
						<Input
							ref="name"
							type="text"
							size="small"
							placeholder="Name"
							onChange={this.handleNameChange}
							style={{ width: 100, marginRight: "5px", marginTop: "5px" }}
						/>
						<Input
							ref="value"
							type="number"
							size="small"
							placeholder="Value"
							onChange={this.handleValueChange}
							style={{ width: 100, marginRight: "5px", marginTop: "5px" }}
						/>
						<Button
							type="button"
							onClick={this.handleInputConfirm}
							style={{ marginTop: "5px" }}>
							Save
						</Button>
					</div>
				)}
				{!inputVisible && (
					<Tag onClick={this.showInput} className="g-btnAdd">
						<Icon type="plus-circle-o" />Add
					</Tag>
				)}
			</div>
		);
	}
}
export default AdditionalInfo;
