import React, { Component } from "react";
import { Button, Icon } from "antd";
import uuid from "uuid";
import CreateStepForm from "./CreateStepForm";
import StepImageUpload from "./StepImageUpload";

class StepCreate extends Component {
	constructor(props) {
		super(props);
		this.state = {
			file: {},
			inputVisible: false,
			additionalInfo: [],
			steps: [],
			step: {}
		};
	}

	onChange = activeKey => {
		this.setState({ activeKey });
	};

	onEdit = (targetKey, action) => {
		this[action](targetKey);
	};

	onAdd = () => {
		this.setState({ inputVisible: true });
	};

	handleFormCancel = () => {
		this.setState({ inputVisible: false });
	};

	handleCreate = e => {
		e.preventDefault();

		const form = this.form;
		form.validateFields((err, values) => {
			if (err) {
				return;
			}
			values._id = uuid.v4();
			values.stepImgList = this.state.imageList;
			values.additionalInfo = this.state.additionalInfo;
			form.resetFields();
			this.setState({
				inputVisible: false,
				steps: [...this.state.steps, values],
				step: {},
				additionalInfo: []
			});
			this.props.onSaveStep(values, false);
		});
	};

	saveFormRef = form => {
		this.form = form;
	};

	handleAdditionalInfo = additionalInfo => {
		this.setState({ additionalInfo });
	};

	handleChange = imageList => {
		this.setState({ imageList });
	};

	handleFormValueChage = (fieldObj, step) => {
		const value = fieldObj[Object.keys(fieldObj)[0]];
		const key = Object.keys(fieldObj)[0];

		//let step = { ...this.state.step };
		step[key] = value;
		this.setState({ step });
	};

	renderForm = () => {
		const { additionalInfo } = this.state;
		return (
			<div className="row g-step-border">
				{/* <StepImage isEdit={true} imageFile={this.handleImageFile} /> */}
				<div id="step" className="col s12 m6 l6 xl6">
					<div>
						<StepImageUpload onChange={this.handleChange} imageUrl={[]} />
					</div>
				</div>
				<div className="col s12 m6 l6 xl6">
					<CreateStepForm
						step={this.state.step}
						onFormValueChange={this.handleFormValueChage}
						onSaveStep={this.handleCreate}
						ref={this.saveFormRef}
						handleAdditionalInfo={this.handleAdditionalInfo}
						onCancelEdit={this.handleFormCancel}
						additionalInfo={additionalInfo}
					/>
				</div>
			</div>
		);
	};

	render() {
		const { inputVisible } = this.state;

		return (
			<div>
				{inputVisible && this.renderForm()}
				{!inputVisible && (
					<div className="g-div-align-item-center">
						<Button onClick={this.onAdd} className="g-btn-g-color">
							<Icon type="plus-circle-o" />Add Step
						</Button>
					</div>
				)}
			</div>
		);
	}
}

export default StepCreate;
