import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import _ from "lodash";
import * as actions from "../../../actions";
import { Input, Button, Icon, Form, Modal, Radio, Tabs } from "antd";
import CreateTimeline from "./CreateTimeline";
import CreateStep from "./CreateStep";
import StepList from "./StepList";
import { LangCode } from "../../../const/LangCode";
import { IMAGE_URL } from "../../../const/ImageUrl";
import TripImageUpload from "./TripImageUpload";

const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const TabPane = Tabs.TabPane;
const { TextArea } = Input;

const LangKey = {
	Japan: "ja",
	English: "en",
	China: "ch",
	Korea: "kr"
};

const CreateTripForm = Form.create({
	onValuesChange(props, changedFields) {
		props.onFormValueChange(changedFields, props.trip);
	},
	mapPropsToFields(props) {
		return {
			title: Form.createFormField({
				value: props.trip.title
			}),
			locationName: Form.createFormField({
				value: props.trip.locationName
			}),
			duration: Form.createFormField({
				value: props.trip.duration
			}),
			durationUnit: Form.createFormField({
				value: props.trip.durationUnit
			}),
			minimumPeople: Form.createFormField({
				value: props.trip.minimumPeople
			}),
			price: Form.createFormField({
				value: props.trip.price
			}),
			currencyCode: Form.createFormField({
				value: props.trip.currencyCode
			}),
			description: Form.createFormField({
				value: props.trip.description
			})
		};
	}
})(props => {
	const { onCreate, onCancel, form, btnSavelbl } = props;
	const { getFieldDecorator } = form;
	return (
		<Form onSubmit={onCreate}>
			<div className="row g-margin-b0 col s12">
				<FormItem label="Title">
					{getFieldDecorator("title", {
						rules: [{ required: true, message: "Please type Title!" }]
					})(<Input />)}
				</FormItem>
			</div>
			<div className="row g-margin-b0 col s12">
				<FormItem label="Location Name">
					{getFieldDecorator("locationName", {
						rules: [
							{
								required: true,
								message: "Please input Location Name"
							}
						]
					})(<Input />)}
				</FormItem>
			</div>
			<div className="row g-margin-b0">
				<div className="col s7">
					<FormItem label="Duration">
						{getFieldDecorator("duration", {
							rules: [
								{
									required: true,
									message: "Please input duration!"
								}
							]
						})(<Input type="number" />)}
					</FormItem>
				</div>
				<div className="col s5">
					<FormItem label="Unit">
						{getFieldDecorator("durationUnit", {
							rules: [
								{
									required: true,
									message: "Please input unit!"
								}
							]
						})(<Input />)}
					</FormItem>
				</div>
			</div>
			<div className="row g-margin-b0 col s12">
				<FormItem label="Minimum People">
					{getFieldDecorator("minimumPeople", {
						rules: [
							{
								required: true,
								message: "Please input Minimum People!"
							}
						]
					})(<Input type="number" />)}
				</FormItem>
			</div>
			<div className="row g-margin-b0">
				<div className="col s7">
					<FormItem label="Price">
						{getFieldDecorator("price", {
							rules: [
								{
									required: true,
									message: "Please input price!"
								}
							]
						})(<Input type="number" />)}
					</FormItem>
				</div>
				<div className="col s5">
					<FormItem label="Currency Code">
						{getFieldDecorator("currencyCode", {
							rules: [
								{
									required: true,
									message: "Please input Currency Code!"
								}
							]
						})(<Input />)}
					</FormItem>
				</div>
			</div>
			<div className="row g-margin-b0 col s12">
				<FormItem label="Description">
					{getFieldDecorator("description", {
						rules: [
							{
								required: true,
								message: "Please input description!"
							}
						]
					})(<TextArea rows={4} />)}
				</FormItem>
			</div>
			<div className="row g-margin-b0 col s12 g-margin-t8">
				<FormItem>
					<Button
						type="primary"
						htmlType="submit"
						className="g-btn-submit right">
						{btnSavelbl}
					</Button>
					<Button onClick={onCancel} className="right g-margin-r8">
						Cancel
					</Button>
				</FormItem>
			</div>
		</Form>
	);
});

class CreateTrip extends Component {
	state = {
		value: "ja",
		title: "Japan",
		isLangAlready: false,
		visibleModal: false,
		panes: [],
		activeKey: "ja",
		timelines: [],
		fileList: [],
		uploading: false,
		imageList: [],
		previewVisible: false,
		previewImage: "",
		trips: [{ langCode: "ja" }],
		form: {},
		forms: [],
		backupTrips: [],
		backupImageList: [],
		backupFileList: []
	};

	componentDidMount() {
		const { editTripId } = this.props;
		if (editTripId) {
			this.props
				.fetchTripDetail({
					tripId: editTripId,
					userId: this.props.user.userId
				})
				.then(() => {
					const nextTrip = this.props.trip;
					if (nextTrip) {
						const trips = nextTrip.trip;

						const file = {
							uid: -1,
							name: "trip.png",
							status: "done",
							url: IMAGE_URL + nextTrip.imageList
						};
						const panes = [];
						if (trips) {
							trips.map(trip => {
								const key = trip.langCode;
								const title = LangCode.find(lang => {
									return lang.code === key;
								});
								return panes.push({
									title: title.name,
									content: this.createForm(trip, key),
									key: key
								});
							});
						}
						this.setState({
							trips,
							imageList: nextTrip.imageList,
							previewImage: IMAGE_URL + nextTrip.imageList,
							fileList: [...this.state.fileList, file],
							panes,
							backupTrips: [...trips],
							backupImageList: nextTrip.imageList,
							backupFileList: [...this.state.fileList, file]
						});
					}
				});
		}
		this.setState({
			panes: [
				{
					title: "Japan",
					content: this.createForm({}, "ja"),
					key: "ja",
					closable: false
				}
			]
		});
	}

	handleTimeline = timelines => {
		let tripIndex = this.state.trips.findIndex(trip => {
			return trip.langCode === this.state.activeKey;
		});
		let tripObj = {};
		if (tripIndex > -1) {
			tripObj = {
				...this.state.trips[tripIndex]
			};
		} else {
			//if we not found langCode in array, we need to add it as new one
			tripObj["langCode"] = this.state.activeKey;
			tripIndex = this.state.trips.length;
		}

		tripObj["timeline"] = timelines;

		const trips = [...this.state.trips];
		trips[tripIndex] = tripObj;
		this.setState({ trips });
	};

	handleDeleteTimeline = index => {
		const timelines = [...this.state.timelines];
		timelines.splice(index, 1);
		this.setState({ timelines });
	};

	handleSaveStep = (step, isEdit) => {
		let tripIndex = this.state.trips.findIndex(trip => {
			return trip.langCode === this.state.activeKey;
		});
		const trips = [...this.state.trips];
		let tripObj = {};
		if (tripIndex > -1) {
			tripObj = {
				...this.state.trips[tripIndex]
			};
		} else {
			//if we not found langCode in array, we need to add it as new one
			tripObj["langCode"] = this.state.activeKey;
			tripIndex = this.state.trips.length;
		}
		if (isEdit) {
			//update step
			const stepIndex = trips[tripIndex].stepList.findIndex(s => {
				return s._id === step._id;
			});

			const steps = [...trips[tripIndex].stepList];
			steps[stepIndex] = step;
			trips[tripIndex].stepList = steps;
		} else {
			//create new step
			if (tripObj.stepList) {
				tripObj.stepList = [...tripObj.stepList, step];
			} else {
				tripObj["stepList"] = [step];
			}
			trips[tripIndex] = tripObj;
		}

		this.setState({ trips });
	};

	handleFormValueChage = (fieldObj, trip) => {
		const value = fieldObj[Object.keys(fieldObj)[0]];
		const key = Object.keys(fieldObj)[0];

		let tripIndex = this.state.trips.findIndex(trip => {
			return trip.langCode === this.state.activeKey;
		});
		let tripObj = {};
		if (tripIndex > -1) {
			tripObj = {
				...this.state.trips[tripIndex]
			};
		} else {
			//if we not found langCode in array, we need to add it as new one
			tripObj["langCode"] = this.state.activeKey;
			tripIndex = this.state.trips.length;
		}

		tripObj[key] = value;

		const trips = [...this.state.trips];
		trips[tripIndex] = tripObj;
		this.setState({ trips });
	};

	createForm = (trip, key) => {
		const btnSavelbl = this.props.editTripId ? "Update Trip" : "Save Trip";
		return (
			<div>
				<div className="row">
					<div className="col m9 l9 xl9 s12">
						<CreateTripForm
							trip={trip}
							onFormValueChange={this.handleFormValueChage}
							ref={this.saveFormRef}
							onCancel={this.handleCancelTripForm}
							onCreate={this.handleCreate}
							btnSavelbl={btnSavelbl}
						/>
					</div>
					<div className="col m3 l3 xl3 s12 offset-s1">
						<CreateTimeline
							timelines={trip.timeline}
							onSaveTimeline={this.handleTimeline}
							onDelete={this.handleDeleteTimeline}
						/>
					</div>
				</div>
			</div>
		);
	};

	onTabChange = activeKey => {
		const forms = [...this.state.forms];
		const formIndex = forms.findIndex(fo => {
			return fo.langCode === this.state.activeKey;
		});
		this.saveFormRef(forms[formIndex].form);
		this.setState({ activeKey });
	};

	onEdit = (targetKey, action) => {
		const { panes } = this.state;
		const trips = [...this.state.trips];
		// we not allow to delete all language panes
		if (panes.length === 1) return;
		const paneIndex = panes.findIndex(pane => {
			return pane.key === targetKey;
		});
		panes.splice(paneIndex, 1);
		trips.splice(paneIndex, 1);
		const index = panes.length - 1;
		const activeKey = panes[index].key;
		this.setState({ panes, activeKey, trips });
	};

	add = () => {
		const { panes, value } = this.state;
		const paneIndex = panes.findIndex(pane => {
			return pane.key === value;
		});
		//if the language pane is already selected, we ignore it
		if (paneIndex >= 0) {
			this.setState({ isLangAlready: true });
			return;
		}
		const activeKey = value;

		panes.push({
			title: this.state.title,
			content: this.createForm({}, activeKey),
			key: activeKey
		});
		const trip = { langCode: activeKey };

		this.setState({
			visibleModal: false,
			panes,
			activeKey,
			isLangAlready: false,
			trips: [...this.state.trips, trip]
		});
	};

	showModal = () => {
		this.setState({
			visibleModal: true
		});
	};

	handleCloseModal = () => {
		this.setState({
			visibleModal: false
		});
	};

	handleCancelTripForm = () => {
		const backupTrips = this.state.backupTrips;
		const backupImageList = this.state.backupImageList;
		const backupFileList = this.state.backupFileList;
		let panes = [];
		let key = "";
		//for edit, we copy old values to state
		if (backupTrips.length > 0) {
			backupTrips.map(trip => {
				const key = trip.langCode;
				const title = LangCode.find(lang => {
					return lang.code === key;
				});
				return panes.push({
					title: title.name,
					content: this.createForm(trip, key),
					key: key
				});
			});
			key = panes.length > 0 ? panes[0].key : "ja";
		} else {
			// for create, we remove all fields in the active tab
			key = this.state.activeKey;
			const oldPanes = this.state.panes;
			let activePaneIndex = oldPanes.findIndex(pane => {
				return pane.key === key;
			});
			let activePane = oldPanes[activePaneIndex];
			activePane.content = this.createForm({}, key);
			oldPanes[activePaneIndex] = activePane;
			//copy array to array
			panes = oldPanes.slice();
		}

		this.setState({
			trips: backupTrips,
			fileList: backupFileList,
			imageList: backupImageList,
			panes,
			activeKey: key
		});
	};

	handleCreate = e => {
		e.preventDefault();
		const forms = [...this.state.forms];

		let isFormValid = true;
		forms.map(fm => {
			return fm.form.validateFields((err, values) => {
				if (err) {
					const activeKey = fm.langCode;
					this.setState({ activeKey });
					isFormValid = false;
				}
			});
		});

		if (isFormValid) {
			const trips = [...this.state.trips];
			let tripObj = {};
			tripObj.userId = this.props.user.userId;
			tripObj.imageList = this.state.imageList;
			trips.map(trip => {
				if (trip.timeline) {
					trip.timeline.filter(time => {
						return delete time._id;
					});
				} else {
					return (trip.timeline = []);
				}
				if (trip.stepList) {
					trip.stepList.filter(step => {
						return delete step._id;
					});
				} else {
					return (trip.stepList = []);
				}
			});
			tripObj.trip = trips;
			if (this.props.editTripId) {
				tripObj.tripId = this.props.editTripId;
				this.props.updateTrip(tripObj, this.props.history);
			} else {
				this.props.createTrip(tripObj, this.props.history);
			}
		}
	};

	saveFormRef = form => {
		const forms = [...this.state.forms];
		const key = this.state.activeKey;
		const formIndex = forms.findIndex(fo => {
			return fo.langCode === key;
		});
		if (form) {
			if (formIndex > -1) {
				let formObj = forms[formIndex];
				formObj.form = form;
				forms[formIndex] = formObj;
				this.setState({ forms });
			} else {
				let formObj = { form: form, langCode: key };
				this.setState({ forms: [...this.state.forms, formObj] });
			}
		}
	};

	onSelectLanguage = e => {
		const val = e.target.value;
		const title = LangCode.find(lang => {
			return lang.code === val;
		});
		this.setState({
			value: val,
			title: title.name,
			isLangAlready: false
		});
	};

	handleDeleteStep = step => {
		let tripIndex = this.state.trips.findIndex(trip => {
			return trip.langCode === this.state.activeKey;
		});
		const trips = [...this.state.trips];
		const trip = trips[tripIndex];
		const steps = [...trip.stepList];
		const index = steps.indexOf(step);
		steps.splice(index, 1);
		trips[tripIndex].stepList = steps;

		this.setState({ trips });
	};

	handleChange = imageList => {
		this.setState({ imageList });
	};

	handleCancelStep = (step, backupStep) => {
		const tripIndex = this.state.trips.findIndex(
			trip => trip.langCode === this.state.activeKey
		);
		const trips = [...this.state.trips];
		const tr = tripIndex > -1 ? trips[tripIndex] : [];

		const stepList = [...tr.stepList];
		const stepIndex = stepList.findIndex(stp => {
			return stp === step;
		});

		stepList[stepIndex] = backupStep;
		trips[tripIndex].stepList = stepList;
		this.setState({ trips });
	};

	render() {
		const { previewImage } = this.state;

		const tripIndex = this.state.trips.findIndex(
			trip => trip.langCode === this.state.activeKey
		);
		const trips = [...this.state.trips];
		const tr = tripIndex > -1 ? trips[tripIndex] : [];
		const panes = [...this.state.panes];
		return (
			<div>
				<div
					className="container"
					id="createTrip"
					style={{ width: "90%", marginBottom: "1em" }}>
					<div>
						<TripImageUpload
							onChange={this.handleChange}
							imageUrl={previewImage}
						/>
					</div>
				</div>
				<div className="g-margin-top-bottom">
					<div style={{ marginBottom: 16 }}>
						<Button onClick={this.showModal}>
							<Icon type="plus-circle-o" />Add Languages
						</Button>
					</div>
					<Tabs
						hideAdd
						onChange={this.onTabChange}
						activeKey={this.state.activeKey}
						type="editable-card"
						onEdit={this.onEdit}>
						{panes.map(pane => (
							<TabPane tab={pane.title} key={pane.key}>
								{pane.content}
							</TabPane>
						))}
					</Tabs>
				</div>
				<Modal
					title="Add Language"
					visible={this.state.visibleModal}
					onOk={this.add}
					onCancel={this.handleCloseModal}>
					<RadioGroup onChange={this.onSelectLanguage} value={this.state.value}>
						<Radio value={LangKey.Japan} defaultChecked>
							Japan
						</Radio>
						<Radio value={LangKey.English}>English</Radio>
						<Radio value={LangKey.China}>China</Radio>
						<Radio value={LangKey.Korea}>Korea</Radio>
					</RadioGroup>
					{this.state.isLangAlready ? (
						<p style={{ color: "red" }}>This Language already selected</p>
					) : null}
				</Modal>

				<StepList
					steps={tr.stepList ? tr.stepList : []}
					onDelete={this.handleDeleteStep}
					onCancel={this.handleCancelStep}
					onSaveStep={this.handleSaveStep}
					onSaveAdditionalInfo={this.handleOnSaveAdditionalInfo}
				/>
				<CreateStep onSaveStep={this.handleSaveStep} />
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		user: state.auth,
		trip: _.get(state.trips.trip, "trip"),
		guider: _.get(state.trips.trip, "guider")
	};
}

export default connect(mapStateToProps, actions)(withRouter(CreateTrip));
