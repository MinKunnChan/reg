import React, { Component } from "react";
import Steps from "./Steps";
import CreateStepForm from "./CreateStepForm";
import { IMAGE_URL } from "../../../const/ImageUrl";
import StepImageUpload from "./StepImageUpload";

class StepList extends Component {
	state = {
		isEdit: false,
		additionalInfo: [],
		stepBackup: {},
		step: {},
		file: {}
	};

	onEdit = step => {
		const backup = { ...step };
		this.setState({ step, isEdit: true, stepBackup: backup });
	};
	onCancel = stp => {
		const backup = this.state.stepBackup;
		this.setState({ isEdit: false });
		this.props.onCancel(stp, backup);
	};
	handleCreate = (e, step) => {
		e.preventDefault();
		const form = this.form;
		form.validateFields((err, values) => {
			if (err) {
				return;
			}
			values._id = step._id;
			values.stepImgList = this.state.imageList;

			const additionalInfo = this.state.additionalInfo;
			if (additionalInfo.length > 0) {
				values.additionalInfo = additionalInfo;
			} else {
				//in edit mode, if no addtionalInfo change, we apply the original value to it
				values.additionalInfo = step.additionalInfo;
			}

			form.resetFields();
			this.setState({
				inputVisible: false
			});

			this.props.onSaveStep(values, this.state.isEdit);
		});
	};

	handleSaveAdditionalInfo = additionalInfo => {
		this.setState({ additionalInfo });
	};

	saveFormRef = form => {
		this.form = form;
	};

	handleFormValueChage = (fieldObj, stp) => {
		const value = fieldObj[Object.keys(fieldObj)[0]];
		const key = Object.keys(fieldObj)[0];
		stp[key] = value;
	};

	handleChange = imageList => {
		this.setState({ imageList });
	};

	render() {
		const stepItems = this.props.steps.map((stp, index) => {
			return (
				<div key={index}>
					<div className="row g-step-border">
						{this.state.isEdit && this.state.step === stp ? (
							<div>
								<div id="step" className="col s12 m6 l6 xl6">
									<StepImageUpload
										onChange={this.handleChange}
										imageUrl={IMAGE_URL + stp.stepImgList}
									/>
								</div>
								<div className="col s12 m6 l6 xl6">
									<CreateStepForm
										step={stp}
										onFormValueChange={this.handleFormValueChage}
										ref={this.saveFormRef}
										onCancelEdit={() => this.onCancel(stp)}
										onSaveStep={e => this.handleCreate(e, stp)}
										handleAdditionalInfo={this.handleSaveAdditionalInfo}
									/>
								</div>
							</div>
						) : (
							<div>
								<div className="col s12 m6 l6 xl6">
									<img
										src={IMAGE_URL + stp.stepImgList}
										alt="stepList"
										className="g-preview-img"
									/>
								</div>
								<Steps
									onDelete={this.props.onDelete}
									onEdit={this.onEdit}
									step={stp}
								/>
							</div>
						)}
					</div>
					<div className="g-color">
						<i className="fa fa-arrow-down fa-lg" />
					</div>
				</div>
			);
		});

		return stepItems;
	}
}

export default StepList;
