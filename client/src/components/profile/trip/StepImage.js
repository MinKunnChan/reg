import React, { Component } from "react";
import { Icon, Upload, Modal } from "antd";
import { IMAGE_URL } from "../../../const/ImageUrl";

class StepImage extends Component {
	state = {
		previewVisible: false,
		previewImage: "",
		fileList: [],
		imageList: [],
		inputVisible: false
	};

	componentDidMount() {
		if (this.props.fileList) {
			const file = {
				uid: -1,
				name: "trip.png",
				status: "done",
				url: IMAGE_URL + this.props.fileList[0]
			};

			this.setState({ fileList: [...this.state.fileList, file] });
		}
	}
	handlePreview = file => {
		this.setState({
			previewImage: file.url || file.thumbUrl,
			previewVisible: true
		});
	};

	handleChange = info => {
		let fileList = info.fileList;
		var self = this;
		fileList = fileList.map(file => {
			if (file.response) {
				self.setState({ imageList: file.response });
				file.url = file.response.url;
				this.props.imageFile(file, file.response);
			}
			return file;
		});

		this.setState({ fileList });
	};
	handleCancel = () => this.setState({ previewVisible: false });

	render() {
		const { previewVisible, previewImage, fileList } = this.state;
		const uploadButton = (
			<div>
				<Icon type="plus" />
				<div className="ant-upload-text">Add Image</div>
			</div>
		);
		const action = `/api/photo`;
		return (
			<div id="step" className="col s12 m6 l6 xl6">
				<div className="clearfix">
					<Upload
						action={action}
						listType="picture-card"
						fileList={fileList}
						showUploadList={{
							showRemoveIcon: this.props.isEdit ? true : false
						}}
						onPreview={this.handlePreview}
						onChange={this.handleChange}>
						{fileList.length >= 1 ? null : uploadButton}
					</Upload>
					<Modal
						visible={previewVisible}
						footer={null}
						onCancel={this.handleCancel}>
						<img alt="example" style={{ width: "100%" }} src={previewImage} />
					</Modal>
				</div>
			</div>
		);
	}
}

export default StepImage;
