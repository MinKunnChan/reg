import React, { Component } from "react";
import { Input, Button, Icon, TimePicker, Timeline, Form } from "antd";
import uuid from "uuid";
import moment from "moment";

const format = "HH:mm";
const FormItem = Form.Item;

const TimelineForm = Form.create({
	mapPropsToFields(props) {
		const time = props.timeline.time ? props.timeline.time : "12:00";
		return {
			time: Form.createFormField({
				value: moment(time, format)
			}),
			description: Form.createFormField({
				value: props.timeline.description
			})
		};
	}
})(props => {
	const { getFieldDecorator } = props.form;
	const config = {
		rules: [{ type: "object", required: true, message: "Please select time!" }]
	};
	return (
		<Form onSubmit={props.handleSubmit}>
			<FormItem>
				{getFieldDecorator("time", config)(<TimePicker format={format} />)}
			</FormItem>
			<FormItem>
				{getFieldDecorator("description", {
					rules: [{ required: true, message: "Please input Description!" }]
				})(<Input placeholder="Description..." />)}
			</FormItem>

			<FormItem>
				<Button
					type="primary"
					htmlType="submit"
					className="g-btn-submit right g-margin-t8">
					Save
				</Button>
				<Button
					onClick={props.onCancel}
					className="right g-margin-r8 g-margin-t8">
					Cancel
				</Button>
			</FormItem>
		</Form>
	);
});
class CreateTimeline extends Component {
	state = {
		timelines: [],
		timeline: {},
		inputVisible: false
	};

	componentWillReceiveProps(nextProps) {
		let nextTimelines = nextProps.timelines;
		this.setState({
			timelines: nextTimelines ? nextTimelines : []
		});
	}

	onAdd = () => {
		this.setState({ inputVisible: true, timeline: {} });
	};
	onDelete = index => {
		const timelines = [...this.state.timelines];
		timelines.splice(index, 1);
		this.setState({ timelines });
	};

	handleSubmit = e => {
		e.preventDefault();

		this.form.validateFields((err, values) => {
			if (!err) {
				const timelineIndex = this.state.timelines.findIndex(timeline => {
					return timeline._id === this.state.timeline._id;
				});
				const times = values.time;
				const minute =
					times.minutes() < 10 ? "0" + times.minutes() : times.minute();
				const time = times.hour() + ":" + minute;

				if (timelineIndex !== -1) {
					const timeline = {
						...this.state.timelines[timelineIndex]
					};

					timeline.description = values.description;
					timeline.time = time;

					const timelines = [...this.state.timelines];
					timelines[timelineIndex] = timeline;

					this.setState({
						timeline: {},
						timelines: timelines,
						inputVisible: false
					});
					this.form.resetFields();
					this.props.onSaveTimeline(timelines);
				} else {
					let timelines = [...this.state.timelines];
					const id = uuid.v4();

					let timeObj = {
						_id: id,
						time: time,
						description: values.description
					};

					if (timeObj && timelines.indexOf(timeObj) === -1) {
						timelines = [...timelines, timeObj];
					}

					this.setState({
						timelines: timelines,
						inputVisible: false
					});
					this.form.resetFields();
					this.props.onSaveTimeline(timelines);
				}
			}
		});
	};

	saveFormRef = form => {
		this.form = form;
	};

	handleEdit = (event, timeline) => {
		this.setState({
			inputVisible: true,
			timeline: timeline
		});
	};

	onCancelForm = () => {
		this.setState({
			inputVisible: false
		});
	};

	renderForm = () => {
		return (
			<TimelineForm
				timeline={this.state.timeline}
				onCancel={this.onCancelForm}
				ref={this.saveFormRef}
				handleSubmit={this.handleSubmit}
			/>
		);
	};
	render() {
		let { timelines, inputVisible } = this.state;
		if (!timelines) {
			timelines = [];
		}
		return (
			<div className="g-width200">
				<h6>Add Timeline </h6>
				<div>
					<Timeline>
						{timelines.map((timeline, index) => {
							return (
								<Timeline.Item key={index}>
									{timeline.time} {timeline.description}
									<i
										className="fa fa-cog fa-lg g-level-edit"
										onClick={event => this.handleEdit(event, timeline)}
										aria-hidden="true"
									/>
									<i
										onClick={() => this.onDelete(index)}
										className="fa fa-trash fa-lg g-level-delete"
									/>
								</Timeline.Item>
							);
						})}
					</Timeline>
				</div>
				{inputVisible && this.renderForm()}
				{!inputVisible && (
					<Button onClick={this.onAdd} className="g-btnAdd">
						<Icon type="plus-circle-o" />Add
					</Button>
				)}
			</div>
		);
	}
}

export default CreateTimeline;
