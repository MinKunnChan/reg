import _ from "lodash";
import React, { Component } from "react";
import { Icon, Timeline, Button } from "antd";
import { connect } from "react-redux";
import * as actions from "../../../actions";
import Auxi from "../../../hoc/Auxi";
import { IMAGE_URL } from "../../../const/ImageUrl";

import Step from "./Step";
import Spinner from "../../../ui/Spinner/Spinner";

class TripDetail extends Component {
	state = {
		isFavourite: "",
		onMobile: false
	};

	componentDidMount() {
		this.props
			.fetchTripDetail({
				tripId: this.props.match.params.id,
				userId: this.props.user.userId,
				langCode: "ja"
			})
			.then(() => {
				this.setState({ isFavourite: this.props.trip.isFavourite });
			})
			.catch(err => {
				console.log("fetchTrip Error ", err);
			});
		window.scrollTo(0, 0);

		//	window.addEventListener("resize", () => this.resize());
		this.resize();
	}

	resize = () => {
		this.setState({ onMobile: window.innerWidth < 699 });
	};

	renderTimeline() {
		const timelines = this.props.trip.timeline;
		return (
			<Timeline>
				{_.map(timelines, timeline => {
					return (
						<Timeline.Item key={timeline._id}>
							{timeline.time} {timeline.description}
						</Timeline.Item>
					);
				})}
			</Timeline>
		);
	}

	renderStepList() {
		return _.map(this.props.trip.stepList, (step, index) => {
			return (
				<div key={step._id}>
					<Step step={step} index={++index} onMobile={this.state.onMobile} />
				</div>
			);
		});
	}

	favorite = () => {
		this.props.createFavourite({
			touristId: this.props.user.userId, // current user
			tripId: this.props.trip._id
		});
	};

	unfavorite = () => {
		this.props.removeFavourite({
			touristId: this.props.user.userId, // current user
			tripId: this.props.trip._id
		});
	};

	render() {
		const { trip, guider } = this.props;
		const { status } = this.props.favouriteStatus;

		if (!trip) {
			return (
				<Auxi>
					<Spinner />
					<div style={{ height: 300 }} />
				</Auxi>
			);
		}
		return (
			<div className="container">
				<div className="row" style={{ marginBottom: "8px", marginTop: "8px" }}>
					<div className="col s6">
						<span className="g-caption">{trip.title}</span>
						<div>
							<Icon type="environment-o" />
							{trip.locationName}
						</div>
					</div>
					<div className="col s6">
						{trip.userId !== this.props.user.userId && this.props.user ? (
							<Button
								icon={
									(this.state.isFavourite === 1 && status === 1) || status === 1
										? "heart-o"
										: "heart"
								}
								className="g-btn btn-small right"
								size="small"
								style={{ marginTop: "1.5em" }}
								onClick={
									(this.state.isFavourite === 1 && status === 1) || status === 1
										? this.unfavorite
										: this.favorite
								}>
								{(this.state.isFavourite === 1 && status === 1) || status === 1
									? "not favourite"
									: "favourite"}
							</Button>
						) : null}
					</div>
				</div>

				<img
					alt="example"
					style={{ width: "100%" }}
					src={IMAGE_URL + trip.imageList[0]}
				/>
				<div className="g-margin-top-bottom">
					<Icon type="camera" /> Nature
				</div>
				<h5>Tomolog By...</h5>
				<fieldset className="g-fieldset">
					<legend className="g-legend">
						<div className="g-img-legend">
							<img
								alt="example"
								className="g-legend-img"
								src={IMAGE_URL + guider.profileImg}
							/>
						</div>
					</legend>
					<div className="g-margin-lr">
						<h6>{guider.name}</h6>
						<p>Locals</p>
						<hr />
						<p className="g-margin-lr">{trip.description}</p>
					</div>
				</fieldset>

				<div className="row g-margin-top-bottom">
					<div className="col s12 m5 l5 xl5">
						<h5>Timeline</h5>
						{this.renderTimeline()}
					</div>
					<div className="col s12 m7 l7 xl7">
						<div className="g-trip-box">
							<p> Number of people: {trip.minimumPeople} (friends)</p>
							<p>
								Total Cost : {trip.currencyCode} {trip.price}
							</p>
							<p>
								Duration : {trip.duration} {trip.durationUnit}
							</p>
						</div>
						<span className="g-caption">Trip Highlight</span>
						<div className="g-img-legend">
							<img
								alt="example"
								className="g-img-40px"
								src={IMAGE_URL + guider.profileImg}
							/>
						</div>
						<p>{trip.description}</p>
					</div>
				</div>
				{this.renderStepList()}
				<div style={{ margin: "1em" }} />
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		user: state.auth,
		trip: _.get(state.trips.trip, "trip"),
		guider: _.get(state.trips.trip, "guider"),
		favouriteStatus: state.favouriteStatus
	};
}

export default connect(mapStateToProps, actions)(TripDetail);
