import React, { Component } from "react";
import { Upload, Icon, Modal } from "antd";
import { connect } from "react-redux";
import * as actions from "../../actions";

class ProfileImage extends Component {
	state = {
		previewVisible: false,
		previewImage: "",
		fileList: []
	};
	componentDidMount() {
		const file = {
			uid: -1,
			name: "xxx.png",
			status: "done",
			url: this.props.data.profileImg
		};
		this.setState({ fileList: [...this.state.fileList, file] });
	}
	
	handleCancel = () => this.setState({ previewVisible: false });

	handlePreview = file => {
		this.setState({
			previewImage: file.url || file.thumbUrl,
			previewVisible: true
		});
	};

	handleChange = info => {
		let fileList = info.fileList;
		fileList = fileList.map(file => {
			this.props.fetchUser();
			if (file.response) {
				//this.setState({ fileList: [...this.state.fileList, file] });
				this.setState({ fileList: file.response });
				// Component will show file.url as link
				file.url = file.response.url;
			}
			return file;
		});

		this.setState({ fileList });
	};

	render() {
		const { previewVisible, previewImage, fileList } = this.state;
		const action = `/api/profileImage/${this.props.data.userId}`;

		const uploadButton = (
			<div>
				<Icon type="plus" />
				<div className="ant-upload-text">Upload</div>
			</div>
		);
		return (
			<div className={this.props.isCollapse ? "clearfix-small" : "clearfix"}>
				<Upload
					action={action}
					listType="picture-card"
					fileList={fileList}
					onPreview={this.handlePreview}
					onChange={this.handleChange}>
					{fileList.length >= 1 ? null : uploadButton}
				</Upload>
				<Modal
					visible={previewVisible}
					footer={null}
					onCancel={this.handleCancel}>
					<img alt="example" style={{ width: "100%" }} src={previewImage} />
				</Modal>
			</div>
		);
	}
}
export default connect(null, actions)(ProfileImage);
