import React, { Component } from "react";
import { Layout, Menu, Icon } from "antd";
import { connect } from "react-redux";
import * as actions from "../../actions";
import Profile from "./Profile";
import Payment from "../payment/Payment";
import FollowerWrapper from "../follower/FollowerWrapper";
import FollowerDashboard from "../chat/FollowerDashboard";
import CreateTrip from "./trip/CreateTrip";
import { GUIDER_ROLE_ID } from "../../const";
import ImageUpload from "../../ui/ImageUpload/ImageUpload";
import { IMAGE_URL } from "../../const/ImageUrl";
import UpdateProfile from "./UpdateProfile";

const { Header, Sider, Content } = Layout;

const MenuKey = {
	MY_PAGE: "my page",
	CREATE_TRIP: "create trip",
	MESSAGE: "message",
	FOLLOWER: "follower",
	PAYMENT: "payment"
};

class ProfileDashboard extends Component {
	state = {
		collapsed: false,
		renderContent: "",
		current: MenuKey.MY_PAGE,
		onMediumScreen: false
	};

	componentDidMount() {
		this.props.fetchUserProfileInfo({
			userId: this.props.user.userId,
			roleId: this.props.user.roleId,
			langCode: "ja"
		});

		const content = <Profile onEditTrip={this.handleEditTrip} />;
		this.setState({ renderContent: content });
		//window.addEventListener("resize", this.resize.bind(this));
		this.resize();
	}
	resize = () => {
		this.setState({
			onMediumScreen: window.innerWidth < 1025,
			collapsed: window.innerWidth < 1025
		});
	};
	toggleCollapsed = () => {
		this.setState({
			collapsed: !this.state.collapsed
		});
	};

	renderChatContent = () => {
		const content = (
			<div className="row g-margin-b0">
				<FollowerDashboard />
			</div>
		);
		this.setState({ renderContent: content, current: MenuKey.MESSAGE });
	};
	updateContent = () => {
		this.setState({ renderContent: <CreateTrip /> });
	};
	renderCreateTrip = () => {
		const content =
			GUIDER_ROLE_ID === this.props.profile.roleId ? (
				<CreateTrip />
			) : (
				<UpdateProfile
					changeToguider={true}
					isOwner={true}
					updateContent={this.updateContent}
				/>
			);
		this.setState({ renderContent: content, current: MenuKey.CREATE_TRIP });
	};
	handleEditTrip = trip => {
		const content = <CreateTrip editTripId={trip} />;
		this.setState({ current: MenuKey.CREATE_TRIP, renderContent: content });
	};
	onSelectMenu = item => {
		switch (item.key) {
			case MenuKey.MY_PAGE:
				return this.setState({
					current: MenuKey.MY_PAGE,
					renderContent: <Profile onEditTrip={this.handleEditTrip} />
				});
			case MenuKey.CREATE_TRIP:
				return this.renderCreateTrip();
			case MenuKey.MESSAGE:
				return this.renderChatContent();
			case MenuKey.FOLLOWER:
				return this.setState({
					current: MenuKey.FOLLOWER,
					renderContent: <FollowerWrapper />
				});
			case MenuKey.PAYMENT:
				return this.setState({
					current: MenuKey.PAYMENT,
					renderContent: <Payment />
				});
			default:
				return this.setState({
					current: MenuKey.MY_PAGE,
					renderContent: <Profile onEditTrip={this.handleEditTrip} />
				});
		}
	};
	render() {
		return (
			<Layout>
				<Sider
					style={{
						overflow: "auto",
						height: "90vh",
						position: "fixed",
						left: 0
					}}
					trigger={null}
					collapsible
					collapsed={this.state.collapsed}>
					<div
						id="logo"
						className={this.state.collapsed ? "logo-small" : "logo"}>
						{/* <ProfileImage
							isCollapse={this.state.collapsed}
							data={this.props.user}
						/> */}
						<ImageUpload
							previewClass="g-preview-img"
							imageUrl={IMAGE_URL + this.props.user.profileImg}
							userId={this.props.user.userId}
							isCollapsed={this.state.collapsed}
							imgClass={
								this.state.collapsed
									? "g-user-profile-img-small"
									: "g-user-profile-img"
							}
						/>
						<div className="g-margin-l15em">
							<strong>{this.props.user.name} </strong>
						</div>
					</div>
					<Menu
						mode="inline"
						theme="light"
						onSelect={this.onSelectMenu}
						selectedKeys={[this.state.current]}>
						<Menu.Item key={MenuKey.MY_PAGE}>
							<Icon type="home" />
							<span>My Page</span>
						</Menu.Item>
						<Menu.Item key={MenuKey.CREATE_TRIP}>
							<Icon type="form" />
							<span>Create Trip</span>
						</Menu.Item>
						<Menu.Item key={MenuKey.MESSAGE}>
							<Icon type="message" />
							<span>Message</span>
						</Menu.Item>
						<Menu.Item key={MenuKey.FOLLOWER}>
							<Icon type="team" />
							<span>Follow / Follower</span>
						</Menu.Item>
						<Menu.Item key={MenuKey.PAYMENT}>
							<Icon type="inbox" />
							<span>Payment Management</span>
						</Menu.Item>
					</Menu>
				</Sider>
				<Layout
					className={
						this.state.collapsed ? "g-collapse-small" : "g-collapse-large"
					}>
					<Header style={{ background: "#fff", padding: 0 }}>
						<Icon
							style={this.state.onMediumScreen ? { cursor: "not-allowed" } : {}}
							className="trigger"
							type={this.state.collapsed ? "menu-unfold" : "menu-fold"}
							onClick={!this.state.onMediumScreen ? this.toggleCollapsed : null}
						/>
					</Header>
					<Content
						style={{
							margin: "8px 0px",
							padding: 16,
							background: "#fff",
							minHeight: 280,
							overflow: "initial"
						}}>
						{this.state.renderContent}
					</Content>
				</Layout>
			</Layout>
		);
	}
}

function mapStateToProps(state) {
	return {
		user: state.auth,
		profile: state.profile
	};
}

export default connect(mapStateToProps, actions)(ProfileDashboard);
