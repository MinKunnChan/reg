import _ from "lodash";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Rate, Button, Icon } from "antd";
import * as actions from "../../actions";
import { IMAGE_URL } from "../../const/ImageUrl";

import TripList from "./trip/TripList";

import ImageList from "./gallery/ImageList";

class GuiderProfile extends Component {
	state = {
		isOwner: false,
		followText: "",
		guiderId: "",
		isFollow: ""
	};

	componentDidMount() {
		let guiderId = this.props.match.params.id;
		let isFollow = this.props.profile.isFollow;
		this.setState({ guiderId, isFollow });
		this.props.fetchGuider({
			guiderId: guiderId,
			userId: this.props.user.userId,
			langCode: "ja"
		});

		this.props.fetchTripList({
			userId: guiderId,
			langCode: "ja"
		});

		this.props.getGalleryAndFirstImage({
			userId: guiderId
		});
		window.scrollTo(0, 0);
	}

	renderLangSkill() {
		return _.map(this.props.profile.langSkill, (lang, index) => {
			return (
				<div key={index}>
					<div className="row g-margin-b1">
						<div className="col s12 m12 l4 xl4">
							<Button type="primary" className="g-width-full">
								{lang.langName}
							</Button>
						</div>
						<div className="col s12 m12 l8 xl8">
							<div className="ant-rate-text g-float-r">{lang.skillName}</div>
							<Rate
								character={<i className="fa fa-minus" aria-hidden="true" />}
								disabled
								value={lang.skillLevel}
								className="g-rate"
							/>
						</div>
					</div>
				</div>
			);
		});
	}

	renderCertifiedList() {
		return _.map(this.props.profile.certifiedList, certicate => {
			return (
				<div key={certicate} className="g-inline">
					<Button className="g-margin-r1 g-btn-default" size="small">
						{certicate}
					</Button>
				</div>
			);
		});
	}

	follow = () => {
		this.props.follow({
			touristId: this.props.user.userId, // current user
			guiderId: this.state.guiderId
		});
	};

	unfollow = () => {
		this.props.unfollow({
			touristId: this.props.user.userId, // current user
			guiderId: this.state.guiderId
		});
	};

	render() {
		const { status } = this.props.followStatus;
		const { greetingList } = this.props.profile;
		let greetingDetail = "";
		if (greetingList) {
			const greeting = greetingList.find(greet => greet.langCode === "ja");
			greetingDetail = greeting.greetingDetail;
		}

		return (
			<div className="g-margin-t1">
				<div className="container">
					<h5>
						<Icon type="retweet" className="g-color" />
						Guider Profile
					</h5>
				</div>
				<div className="g-detail shadow">
					<div className="container" style={{ width: "85%" }}>
						<div className="row g-padding">
							<div className="col s12 m1 l1 xl1" style={{ paddingLeft: "4px" }}>
								<img
									alt="profile"
									className="circle g-img right"
									src={IMAGE_URL + this.props.profile.profileImg}
								/>
								{this.props.user.userId !== this.props.profile.userId &&
								this.props.user.userId ? (
									<Button
										onClick={
											(this.state.isFollow === 1 && status === 1) ||
											status === 1
												? this.unfollow
												: this.follow
										}
										type="primary"
										size="small"
										style={{ marginTop: ".5em" }}>
										{(this.state.isFollow === 1 && status === 1) || status === 1
											? "Unfollow"
											: "Follow"}
									</Button>
								) : null}
							</div>
							<div className="col s12 m7 l7 xl7">
								<strong className="g-space-r1">
									{this.props.profile.name}
								</strong>
								<Rate
									className="g-star-color"
									disabled
									value={this.props.profile.rating}
								/>
								<strong>Reviews({this.props.profile.reviewCount})</strong>
								<p>{greetingDetail}</p>
							</div>
							<div className="col s12 m4 l4 xl4 verticalLine">
								{this.renderLangSkill()}
								<div className="g-margin-t1">{this.renderCertifiedList()}</div>
							</div>
						</div>
					</div>
				</div>
				<div className="container">
					<TripList isOwner={this.state.isOwner} />
					<ImageList />
					<div className="g-extra-block" />
					{/* <ReviewList /> */}
				</div>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		profile: state.profile,
		user: state.auth,
		followStatus: state.followStatus
	};
}

export default connect(mapStateToProps, actions)(GuiderProfile);
