import React, { Component } from "react";
import { Tag, Input, Tooltip, Icon } from "antd";

class CertificateForm extends Component {
	state = {
		certifiedList: [],
		inputVisible: false,
		inputValue: ""
	};

	showInput = () => {
		this.setState({ inputVisible: true }, () => this.input.focus());
	};

	handleInputChange = e => {
		this.setState({ inputValue: e.target.value });
	};

	handleInputConfirm = () => {
		const inputValue = this.state.inputValue;
		const propsValue = this.props.certifiedList ? this.props.certifiedList : [];
		let certifiedList =
			this.state.certifiedList.length > 0
				? this.state.certifiedList
				: propsValue;
		if (inputValue) {
			certifiedList = [...certifiedList, inputValue];
		}
		this.setState({
			certifiedList,
			inputVisible: false,
			inputValue: ""
		});

		this.props.onCreateCertificate(certifiedList);
	};

	saveInputRef = input => (this.input = input);

	render() {
		const { inputVisible, inputValue } = this.state;
		const certifiedList =
			this.state.certifiedList.length > 0
				? this.state.certifiedList
				: this.props.certifiedList;
		let certifiedContent = null;

		if (certifiedList) {
			certifiedContent = certifiedList.map((tag, index) => {
				const isLongTag = tag.length > 25;
				const tagElem = (
					<Tag
						className="g-btnAdd"
						key={index}
						closable={true}
						onClose={e => this.props.onDeleteCertificate(e, index)}>
						{isLongTag ? `${tag.slice(0, 25)}...` : tag}
					</Tag>
				);
				return isLongTag ? (
					<Tooltip title={tag} key={index}>
						{tagElem}
					</Tooltip>
				) : (
					tagElem
				);
			});
		}
		return (
			<div className="row">
				<span className="g-span-label">Certificate</span>
				{certifiedContent}
				{inputVisible && (
					<Input
						ref={this.saveInputRef}
						type="text"
						size="small"
						style={{ width: 120 }}
						value={inputValue}
						onChange={this.handleInputChange}
						onBlur={this.handleInputConfirm}
						onPressEnter={this.handleInputConfirm}
					/>
				)}
				{!inputVisible && (
					<Tag onClick={this.showInput} className="g-btnAdd">
						<Icon type="plus-circle-o" className="fa" />Add
					</Tag>
				)}
			</div>
		);
	}
}

export default CertificateForm;
