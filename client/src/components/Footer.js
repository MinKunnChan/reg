import React from "react";

const Footer = () => {
	const year = new Date().getFullYear();
	return (
		<div>
			<div className="g-footer" />
			<div className="g-copyright">&copy;{year} Guider All right Reserved.</div>
		</div>
	);
};

export default Footer;
