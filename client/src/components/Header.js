import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import Signin from "./auth/Signin";
import Signup from "./auth/Signup";

class Header extends Component {
	renderContent() {
		switch (this.props.auth) {
			case null:
				return;
			case false:
				return (
					<ul className="right">
						<li>
							<span className="g-black g-disable">
								<span
									className="fa fa-user-o g-color fa-lg g-margin-r-20"
									aria-hidden="true"
								/>
								<span className="hide-on-small-only">Guest</span>
							</span>
						</li>
						{/* <li>
							<Link to="">
								<Badge count={5}>
									<span className="fa fa-bell fa-lg" aria-hidden="true" />
								</Badge>
							</Link>
						</li> */}
						<li className="hide-on-small-only g-space-r1">
							<Signin classes="g-btn" />
						</li>
						<li className="hide-on-small-only g-space-r1">
							<Signup classes="g-btn" />
						</li>

						<li className="hide-on-med-and-up g-space-r1">
							<Signin classes="g-btn-link" />
						</li>
						<li className="hide-on-med-and-up g-space-r1">
							<Signup classes="g-btn-link" />
						</li>
					</ul>
				);
			default:
				return (
					<ul className="right">
						<li>
							<Link className="g-black" to="/profile">
								<span
									className="fa fa-user-o g-color fa-lg g-margin-r-20"
									aria-hidden="true"
								/>
								<span
									className="hide-on-small-only"
									style={{ textDecoration: "none" }}>
									{this.props.auth.name}
								</span>
							</Link>
						</li>
						<li>
							<Link to="/signout" className="g-color g-remove-underline">
								Sign Out
							</Link>
						</li>
					</ul>
				);
		}
	}

	render() {
		return (
			<div className="navbar-fixed">
				<nav className="header-bar">
					<div className="g-nav-wrapper white">
						<Link
							// to={this.props.auth ? "/dashboard" : "/"}
							to={"/"}
							className="g-heder left g-remove-underline">
							Guider
						</Link>
						{this.renderContent()}
					</div>
				</nav>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		auth: state.auth
		// user: state.auth.user
	};
}

export default connect(mapStateToProps)(Header);
