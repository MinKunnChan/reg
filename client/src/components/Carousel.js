import React from "react";
import { Carousel } from 'antd';

import img1 from '../image/fuji-mountain-1.jpg';
import img2 from '../image/fuji-mountain-2.jpg';
import img3 from '../image/fuji-mountain-3.jpg';
import img4 from '../image/fuji-mountain-4.jpg';

const Carousels = () => {
  
  return (
    <Carousel autoplay>
      <div><img className="g-carousel-img" alt="example" src={img1} style={{ objectFit: "cover"}}/></div>
      <div><img className="g-carousel-img" alt="example" src={img2} style={{ objectFit: "cover"}}/></div>
      <div><img className="g-carousel-img" alt="example" src={img3} style={{ objectFit: "cover"}}/></div>
      <div><img className="g-carousel-img" alt="example" src={img4} style={{ objectFit: "cover"}}/></div>
    </Carousel>
  );
};

export default Carousels;