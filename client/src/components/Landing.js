import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../actions";
import _ from "lodash";
import { Icon, message } from "antd";
import ReactDOM from "react-dom";
import Carousel from "./Carousel";
import CarouselFooter from "./CarouselFooter";
import RecentSearchListItem from "./RecentSearchListItem";
import RecommendedGuiderListItem from "./RecommendedGuiderListItem";
import RecommendedTripListItem from "./RecommendedTripListItem";
import ContentBottom from "./ContentBottom";
import SearchBar from "./SearchBar";

message.config({
	top: 50,
	duration: 2
});

class Landing extends Component {
	componentDidMount() {
		if (this.props.user.userId) {
			this.props.fetchRecentView({
				userId: this.props.user.userId,
				langCode: "ja"
			});
		}

		this.props.fetchRecommendedGuiders({ langCode: "ja" });
		this.props.fetchRecommendedTrip({ langCode: "ja" });
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.user !== nextProps.user) {
			this.props.fetchRecentView({
				userId: nextProps.user.userId,
				langCode: "ja"
			});
		}
	}

	search = term => {
		this.props
			.fetchSearchResult({ locationName: term, langCode: "ja" })
			.then(res => {
				const { tripList, guiderList } = this.props;
				const searchResult = tripList.concat(guiderList);
				if (searchResult.length === 0) {
					message.error("No Result Found!");
				} else {
					let s = searchResult.length > 1 ? "results" : "result";
					message.success(`Found ${searchResult.length} ${s}`);
					const resultNode = ReactDOM.findDOMNode(this.refs.result);
					window.scrollTo(0, resultNode.offsetTop - 90);
				}
			});
	};

	renderRecentView = () => {
		const recentSearch = this.props.recentSearch;

		if (recentSearch.length > 0) {
			return (
				<div>
					<h5>
						<Icon type="retweet" className="g-color" />
						Recent Search
					</h5>
					<div className="row">
						{_.map(recentSearch, view => {
							return <RecentSearchListItem view={view} key={view.tripId} />;
						})}
					</div>
				</div>
			);
		}
	};

	renderRecommendedGuider = () => {
		const guiders = this.props.guiders;
		if (guiders.length > 0) {
			return (
				<div>
					<h5>
						<Icon type="retweet" className="g-color" />
						Recommended Guider
					</h5>
					<div className="row">
						{_.map(guiders, guider => {
							return (
								<RecommendedGuiderListItem
									guider={guider}
									key={guider.userId}
								/>
							);
						})}
					</div>
				</div>
			);
		}
	};

	renderRecommendedTrip = () => {
		const recommdedTrips = this.props.recommdedTrips;
		if (recommdedTrips.length > 0) {
			return (
				<div>
					<h5>
						<Icon type="retweet" className="g-color" />
						Recommended Trip
					</h5>
					<div className="row">
						{_.map(recommdedTrips, (trip, i) => {
							return <RecommendedTripListItem trip={trip} key={i} />;
						})}
					</div>
				</div>
			);
		}
	};

	renderSearchResult = () => {
		let guiderList = this.props.guiderList || [];
		let tripList  = this.props.tripList || [];
		const searchResult = tripList.concat(guiderList);

		return (
			<div>
				{searchResult.length > 0 ? (
					<div>
						<h5>
							<Icon type="retweet" className="g-color" />
							Search Result (Trip)
						</h5>
						{tripList.length > 0 ? 
							(<div className="row">
								{_.map(tripList, view => {
									return <RecentSearchListItem view={view} key={view.title} />;
								})}
							</div>) :
							<div className="row">No Result Found for Trip!</div> 
						}
						<h5>
							<Icon type="retweet" className="g-color" />
							Search Result (Guider)
						</h5>
						{guiderList.length > 0 ? 
							(<div className="row">
								{_.map(guiderList, guider => {
									return <RecommendedGuiderListItem guider={guider} key={guider.userId} />;
								})}
							</div> ) :
							<div className="row">No Result Found for Guider!</div> 
						}
					</div>
				) : (
					<div />
				)}
			</div>
		);
	};

	render() {
		return (
			<div>
				<div className="g-carousel">
					<Carousel />
					<SearchBar search={this.search} />
				</div>
				<CarouselFooter />
				<div ref="result" className="container">
					{this.renderSearchResult()}
					{this.renderRecentView()}
					{this.renderRecommendedGuider()}
					{this.renderRecommendedTrip()}
					<ContentBottom />
				</div>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		user: state.auth,
		recentSearch: state.recentSearch,
		guiders: state.guides,
		searchResult: state.searchResult,
		tripList: _.get(state.searchResult, "tripList"),
		guiderList: _.get(state.searchResult, "guiderList"),
		recommdedTrips: state.recommdedTrips
	};
}

export default connect(mapStateToProps, actions)(Landing);
