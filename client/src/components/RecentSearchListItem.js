import React from "react";
import { Card } from "antd";
import { Link } from "react-router-dom";
import { IMAGE_URL } from "../const/ImageUrl";

const { Meta } = Card;
const renderDetail = details => {
	const len = details ? details.length : 0;
	const detail = len < 30 ? details : details.slice(0, 30) + "...";
	return <span>{detail}</span>;
};
const RecentSearchListItem = ({ view }) => {
	return (
		<div className="col s12 m6 l3 xl3">
			<div className="g-card-spacing">
				<Link
					to={"tripDetail/" + view.tripId}
					style={{ textDecoration: "none" }}>
					<Card
						hoverable
						cover={
							<img
								className="g-card-img"
								alt={view.title}
								src={IMAGE_URL + view.imageList[0]}
							/>
						}>
						<Meta title={view.title} description={renderDetail(view.slogan)} />
					</Card>
				</Link>
			</div>
		</div>
	);
};

export default RecentSearchListItem;
