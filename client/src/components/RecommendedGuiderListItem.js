import React from "react";
import { Card } from "antd";
import { Link } from "react-router-dom";
import { IMAGE_URL } from "../const/ImageUrl";

const { Meta } = Card;
const renderDetail = greetingDetail => {
	const len = greetingDetail ? greetingDetail.length : 0;
	const detail =
		len < 40 ? greetingDetail : greetingDetail.slice(0, 40) + "...";
	return <span>{detail}</span>;
};
const RecommendedGuiderListItem = ({ guider }) => {
	return (
		<div className="col s12 m6 l3 xl3">
			<div className="g-card-spacing">
				<Link
					to={"profile/" + guider.userId}
					style={{ textDecoration: "none" }}>
					<Card
						hoverable
						cover={
							<img
								className="g-card-img"
								alt="example"
								src={IMAGE_URL + guider.profileImg}
							/>
						}>
						<Meta
							title={guider.name}
							description={renderDetail(guider.greetingDetail)}
						/>
					</Card>
				</Link>
			</div>
		</div>
	);
};

export default RecommendedGuiderListItem;
