import React from "react";
const Aggrement = () => {
	return (
		<div className="container">
			<h4>The copyright notice </h4>
			<p>
				The copyright notice should include the year in which you finished
				preparing the release (so if you finished it in 1998 but didn't post it
				until 1999, use 1998). You should add the proper year for each past
				release; for example, “Copyright 1998, 1999 Terry Jones” if some
				releases were finished in 1998 and some were finished in 1999. If
				several people helped write the code, use all their names. For software
				with several releases over multiple years, it's okay to use a range
				(“2008-2010”) instead of listing individual years (“2008, 2009, 2010”)
				if and only if every year in the range, inclusive, really is a
				“copyrightable” year that would be listed individually; and you make an
				explicit statement in your documentation about this usage. Always use
				the English word “Copyright”; by international convention, this is used
				worldwide, even for material in other languages. The copyright symbol
				“©” can be included if you wish (and your character set supports it),
				but it's not necessary. There is no legal significance to using the
				three-character sequence “(C)”, but it does no harm. If you have copied
				code from other programs covered by the same license, copy their
				copyright notices too. Put all the copyright notices for a file
				together, right near the top of the file.
			</p>
			<h4>The license files </h4>
			<p>
				You should also include a copy of the license itself somewhere in the
				distribution of your program. All programs, whether they are released
				under the GPL or LGPL, should include the text version of the GPL. In
				GNU programs we conventionally put the license in a file called COPYING.
				If you are releasing your program under the GNU AGPL, use the text
				version of the GNU AGPL instead of the GNU GPL. If you are releasing
				your program under the Lesser GPL, you should also include the text
				version of the LGPL, usually in a file called COPYING.LESSER. Please
				note that, since the LGPL is a set of additional permissions on top of
				the GPL, it's crucial to include both licenses so users have all the
				materials they need to understand their rights.
			</p>

			<h4>Why license notices? </h4>
			<p>
				The purpose of a free software license is to give certain rights to all
				users of a program. If it is not clear what rights you have given them,
				that defeats the purpose. Our practices are designed to avoid any
				uncertainty. If a program has a copy of a license FOO alongside the
				source files, but doesn't have an explicit statement that “This program
				is released under license FOO,” that leaves room for uncertainty about
				whether the license FOO applies to the code of that program. If a
				release has one statement that “This program is released under license
				FOO,” in a central place such as the README file, that makes the
				situation clear for that release. However, programmers often copy source
				files from one free program into another. If a source file contains no
				statement about what its license is, then moving it into another context
				eliminates all trace of that point. This invites confusion and error.
			</p>
		</div>
	);
};

export default Aggrement;
