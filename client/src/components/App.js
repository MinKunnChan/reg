import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import { connect } from "react-redux";
import * as actions from "../actions";

import Header from "./Header";
import Footer from "./Footer";
import Landing from "./Landing";
import Signin from "./auth/Signin";
import Signout from "./auth/Signout";
import Signup from "./auth/Signup";
import GuiderProfile from "./profile/GuiderProfile";
import ProfileDashboard from "./profile/ProfileDashboard";
import TripDetail from "./profile/trip/TripDetail";
import RequireAuth from "./auth/RequireAuth";
import Aggrement from "./aggrement/Aggrement";

class App extends Component {
	componentDidMount() {
		this.props.fetchUser();
	}

	render() {
		if (!this.props.user) {
			return (
				<div>
					<BrowserRouter>
						<div>
							<Header />
							<Route exact path="/" component={Landing} />
							<Route exact path="/signin" component={Signin} />
							<Route exact path="/signup" component={Signup} />
							<Route exact path="/signout" component={Signout} />
							<Route exact path="/profile/:id" component={GuiderProfile} />
							<Route exact path="/tripDetail/:id" component={TripDetail} />
							<Footer />
						</div>
					</BrowserRouter>
				</div>
			);
		}
		return (
			<div>
				<BrowserRouter>
					<div>
						<Header />
						<Route exact path="/" component={Landing} />
						<Route exact path="/signin" component={Signin} />
						<Route exact path="/signup" component={Signup} />
						<Route exact path="/signout" component={Signout} />
						<Route exact path="/aggrement" component={Aggrement} />
						<Route
							exact
							path="/profile"
							component={RequireAuth(ProfileDashboard)}
						/>
						<Route exact path="/profile/:id" component={GuiderProfile} />
						<Route exact path="/tripDetail/:id" component={TripDetail} />
						<Footer />
					</div>
				</BrowserRouter>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		user: state.auth
	};
}

export default connect(mapStateToProps, actions)(App);
