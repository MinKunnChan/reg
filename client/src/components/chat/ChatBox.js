import React, { Component } from "react";

class ChatBox extends Component {
	constructor(props) {
		super(props);
		this.state = { value: "" };
	}

	handleChange = event => {
		this.setState({ value: event.target.value });
	};

	handleSubmit = event => {
		event.preventDefault();
		this.props.onSendMessage(this.state.value);
		this.setState({ value: "" });
	};

	render() {
		const {value} = this.state;
		return (
			<form onSubmit={this.handleSubmit}>
				<div className="g-chatbox">
					<input
						type="text"
						value={value}
						onChange={this.handleChange}
						style={{ width: "90%" }}
					/>
					<input
						disabled = { value.length < 1 }
						type="submit"
						value="Send"
						className="ant-btn g-btn-submit right ant-btn-primary"
					/>
				</div>
			</form>
		);
	}
}

export default ChatBox;
