import React, { Component } from "react";
import InfiniteScroll from "react-infinite-scroller";
import { connect } from "react-redux";
import ReactDOM from "react-dom";
import * as actions from "../../actions";
import ChatContent from "./ChatContent";

function onLoadMore(value) {
	// console.log(`Load More ${value}`);
}

class ChatMessage extends Component {
	componentDidMount() {
		const { socket } = this.props;
		socket.on("sendMessage", this.addMessageToChat);
	}
	componentDidUpdate(prevProps, prevState) {
		this.scrollBottom();
	}

	scrollBottom = () => {
		const resultNode = ReactDOM.findDOMNode(this.refs.result);
		if (resultNode) {
			resultNode.scrollIntoView();
		}
	};

	addMessageToChat = message => {
		const { receiver, addMessageToChat } = this.props;
		addMessageToChat(message, receiver);
	};

	render() {
		const { messages, receiver } = this.props;
		const { userId, profileImg } = this.props.user;
		console.log(this.refs);

		return (
			<div>
				<h6 className="g-margin-bb1">{this.props.receiver.name}</h6>

				<div style={{ height: "465px", overflow: "auto" }}>
					<InfiniteScroll
						pageStart={0}
						loadMore={onLoadMore}
						hasMore={true}
						useWindow={false}>
						{messages.map((msg, index) => {
							return (
								<ChatContent
									key={index}
									msg={msg}
									receiverId={receiver.userId}
									receiverImg={receiver.profileImg}
									senderId={userId}
									senderImg={profileImg}
								/>
							);
						})}
						<div ref="result" />
					</InfiniteScroll>
				</div>
				{/* <ChatBox /> */}
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		user: state.auth
	};
}

export default connect(mapStateToProps, actions)(ChatMessage);
