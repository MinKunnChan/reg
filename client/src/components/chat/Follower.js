import React, { Component } from "react";
import InfiniteScroll from "react-infinite-scroller";
import { List, Avatar } from "antd";
import SearchBar from "./SearchBar";
import { connect } from "react-redux";
import * as actions from "../../actions";
import { IMAGE_URL } from "../../const/ImageUrl";

class Followers extends Component {
	constructor(props) {
		super(props);

		this.state = {
			followers: []
		};
	}

	onLoadMore = value => {
		// console.log(`Load More ${value}`);
	};

	searchFollower = term => {
		var followerList = this.props.followers;

		followerList = followerList.filter(item => {
			return item.name.toLowerCase().search(term.toLowerCase()) !== -1;
		});

		this.setState({ followers: followerList });
	};

	render() {
		const data =
			this.state.followers.length > 0
				? this.state.followers
				: this.props.followers;
		return (
			<div>
				<SearchBar onSearch={this.searchFollower} />
				<div
					className="col s12 m6 l6 xl6"
					style={{ height: "455px", overflow: "auto" }}>
					<InfiniteScroll
						pageStart={0}
						loadMore={this.onLoadMore}
						hasMore={true}
						useWindow={false}>
						<List
							itemLayout="horizontal"
							dataSource={data}
							renderItem={(item, index) => (
								<div
									onClick={() => this.props.onClick(item)}
									style={{ cursor: "pointer" }}
									key={index}>
									<List.Item>
										<List.Item.Meta
											avatar={<Avatar src={IMAGE_URL + item.profileImg} />}
											title={item.name}
										/>
									</List.Item>
								</div>
							)}
						/>
					</InfiniteScroll>
				</div>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		user: state.auth
	};
}

export default connect(mapStateToProps, actions)(Followers);
