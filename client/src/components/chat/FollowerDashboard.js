import React, { Component } from "react";
import io from 'socket.io-client';
import Auxi from "../../hoc/Auxi";
import ChatMessage from "./ChatMessage";
import ChatBox from "./ChatBox";
import Follower from "./Follower";
import { connect } from "react-redux";
import * as actions from "../../actions";
import _ from "lodash";
import { SOCKET_URL } from '../../const/index';

class FollowerDashboard extends Component {
	constructor(props) {
		super(props);

		this.state = {
			messages: [],
			receiver: {},
			socket: null,
			showChatbox: false
		};
	}

	componentWillMount() {
		this.initSocket();
	}

	initSocket() {
		const socket = io(SOCKET_URL);
	
		this.setState({ socket });
	}

	componentDidMount() {
		const {socket} = this.state;
		socket.emit('joinRoom', this.props.user.userId);

		this.props.fetchFollwers({ guiderId: this.props.user.userId });
		this.props.fetchFollows({ touristId: this.props.user.userId });
	}

	onClick = item => {
		this.props
			.fetchChattedMessage({
				senderId: this.props.user.userId, // current user
				receiverId: item.userId
			})
			.then(() => {
				this.setState({ 
					messages: this.props.messages,
					receiver: item,
					showChatbox: true
				});
			});
	};

	sendMessage = message => {
		let msgObj = {
			senderId: this.props.user.userId, // current user
			receiverId: this.state.receiver.userId,
			message: message
		};
		this.setState({
			messages: [...this.state.messages, msgObj]
		});
		this.state.socket.emit('sendMessage', msgObj);
	};

	addMessageToChat = (message, receiver)=>{
		if(message.senderId === receiver.userId) {
			this.setState({
				messages: [...this.state.messages, message]
			});
		}
	}

	render() {
		let followers = this.props.followers;
		let followings = this.props.following;
		let concatArray = followers.concat(followings);
		concatArray = _.uniqWith(concatArray, _.isEqual);

		const {messages, receiver, socket, showChatbox} = this.state;

		return (
			<Auxi>
				<div className="col s3 m3 l3 xl3 g-flower-border">
					<div className="g-follwer-bottom">
						<i className="fa fa-user" aria-hidden="true" />
						Contact List
					</div>
					<Follower onClick={this.onClick} followers={concatArray} />
				</div>
				<div className="col s9 m9 l9 xl9">
					<ChatMessage
						messages={messages}
						receiver={receiver}
						socket={socket}
						addMessageToChat={this.addMessageToChat}
					/>
					{ showChatbox ? 
						<ChatBox onSendMessage={this.sendMessage} /> : null
					}
				</div>
			</Auxi>
		);
	}
}

function mapStateToProps(state) {
	return {
		user: state.auth,
		messages: state.messages.all,
		message: state.messages.message,
		followers: state.followers,
		following: state.following
	};
}

export default connect(mapStateToProps, actions)(FollowerDashboard);
