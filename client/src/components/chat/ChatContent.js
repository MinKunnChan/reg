import React from "react";
import moment from "moment";
import { IMAGE_URL } from "../../const/ImageUrl";

const ChatContent = props => {
	let userImg = "";
	let imgClasses = "";
	let textClasses = "";
	let pClass = "";
	let dateClasses = "";
	if (props.senderId === props.msg.senderId) {
		userImg = props.senderImg;
		imgClasses = "g-chat-img";
		textClasses = "bubble-owner";
		pClass = "right-align g-margin-b0";
	} else {
		userImg = props.receiverImg;
		imgClasses = "g-chat-float-right";
		textClasses = "bubble-user";
		pClass = "g-margin-b0";
		dateClasses = "left-align";
	}
	const date = moment(props.msg.sendDate)
		.format("h:mm a");
	return (
		<div className="row g-margin-b8">
			<img alt="example" className={imgClasses} src={IMAGE_URL + userImg} />
			<div className={textClasses}>
				{props.msg.message}
				<p className={pClass}>
					<strong className={dateClasses}>{date}</strong>
				</p>
			</div>
		</div>
	);
};

export default ChatContent;
