import React from "react";
import { Input } from 'antd';
const Search = Input.Search;

const SearchBar = (props) => {

  return (
    <div className="g-follower-search">
        <Search
          placeholder=""
          onSearch={ (value) => props.onSearch(value) }
        />
    </div>
  );
};

export default SearchBar;
