const express = require('express');
const imgRouter = express.Router();
let multer = require('multer');
const {Image} = require('../models/Image');
const {UserInfo} = require('../models/UserInfo');
const {ObjectID} = require('mongodb');
let upload  = multer({ storage: multer.memoryStorage() });

/**
 * Update user profile image.
 * @param {file} file single file
 * @returns HTTP status 200.
 */
imgRouter.post('/profileImage/:userId', upload.single('file'), async function(req, res) {
  const userId = req.params.userId;

  // Bad Request from client
  if (!req.file || !ObjectID.isValid(userId)) {
    return res.status(400).send();
  }

  const userInfo = await UserInfo.findOne({userId: userId});

  // Save new profile image.
  var newItem = new Image();
  newItem.img.data = req.file.buffer;
  newItem.img.contentType = req.file.mimetype;
  newItem.isProfile = true;
  await newItem.save();

  // Delete old profile image.
  await Image.remove({_id: userInfo.profileImg});

  // Update with new profile image.
  const result = await UserInfo.update({'userId': userId}, { $set: {
    profileImg: newItem._id
  }});

  // In case of update successful.
  if(result.n > 0) {
    res.send(result);
  } else {
    // Internal server error.
    res.status(500).send();
  }
});

/**
 * Tempory save user uploaded images. That images can be trip, step of trip.
 * @param {file} file file list
 * @returns Image Id list.
 */
imgRouter.post('/photo', upload.array('file'), function(req, res) {

  // Bad Request from client
  if (!req.files) {
    return res.status(400).send();
  }

  var imageIdList = [];
  for (var file of req.files) {
   var newItem = new Image();
   newItem.img.data = file.buffer;
   newItem.img.contentType = file.mimetype;
   newItem.save();
   imageIdList.push(newItem._id);
  }

 res.send(imageIdList);
});

/**
 * Get all image in all trip of a guider.
 * @param {mongoose.Schema.Types.ObjectId} userId Guider Id
 * @returns {List} Image list.
 */
imgRouter.post('/getUserGallery', async (req, res) => {
  try {
    const userId = req.body.userId;

    if (!ObjectID.isValid(userId)) {
      return res.status(400).send();
    }

    const imgList = await Image.getUserGallery(userId);
    res.send(imgList);
  } catch(e) {
    res.status(400).send(e);
  }
});

// TODO to check authenticate
imgRouter.get('/clear', async function(req, res) {
  await Image.remove({createUser: undefined, isProfile: undefined});
  res.send();
});

// Download image by image id.
imgRouter.get('/photo/:imageId', async function(req, res) {
  // if(!req.user) {
  //   res.status(400).send();
  // }
  var imageId = req.params.imageId;

  if (!ObjectID.isValid(imageId)) {
    return res.status(400).send();
  }

  var results = await Image.findById(imageId);
  res.setHeader('content-type', results.img.contentType);
  res.send(results.img.data);
});

module.exports = imgRouter;
