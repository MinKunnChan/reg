const passport = require('passport');
const Authentication = require('../controller/authentication');
const {User} = require('../models/User');

const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local-login', { session: true });
const requireSignup = passport.authenticate('local-signup', { session: true });

module.exports = app => {
//   app.get('/', requireAuth, function(req, res) {
//     res.send({ message : 'Super Secret code is ABC123' });
//   })

  app.get(
      '/auth/google',
      passport.authenticate('google', {
          scope: ['profile', 'email']
      })
  );

  app.get(
    '/auth/google/callback',
    passport.authenticate('google'),
    (req, res) => {
      res.redirect('/');
    }
  );

  app.get(
      '/auth/facebook',
      passport.authenticate('facebook', { scope : ['email'] })
  );

  app.get(
    '/auth/facebook/callback',
    passport.authenticate('facebook'),
    (req, res) => {
        res.redirect('/');
    }
  );

  // process the login form
  app.post('/api/signin', requireSignin, Authentication.signin);

  app.post('/api/signup', requireSignup, Authentication.signin);

  app.get('/api/logout', (req, res) => {
    req.logout();

    // destroy session data
    req.session = null;

    res.redirect('/');
  });

  app.get('/api/current_user', async (req, res) => {
      var user;
      if(req.user) {
        user = await User.getLoginUserInfo(req.user.id);
      }
      req.user = user;
      res.send(req.user);
  });
}
