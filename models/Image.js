const mongoose = require('mongoose');
var ImageSchema = new mongoose.Schema(
  { img: {
      data: Buffer,
      contentType: String
    },
    createUser: mongoose.Schema.Types.ObjectId,
    createDate: {
      type: Date,
      default: Date.now
    },
    isProfile: Boolean
  }
);

/**
 * Get all image in all trip of a guider.
 * @param {mongoose.Schema.Types.ObjectId} userId Guider Id
 * @returns {List} Image list.
 */
ImageSchema.statics.getUserGallery = async function (userId) {
  var Image = this;
  const imgList = await Image.find({'createUser': userId});
  var resImgList = [];
  for(var imgObj of imgList) {
    resImgList.push(imgObj._id);
  }
  return resImgList;
};

ImageSchema.statics.deleteTempImageInfo = async function (imgList) {
  let updateRecord = 0;
  for(let imgId of imgList) {
    await Image.update(
     { _id: imgId}, { $unset: { createUser: 1, createDate: 1 } });
    updateRecord++;
  }
  return updateRecord;
};

ImageSchema.statics.updateImageInfo = async function (userId, imgList) {

  let updateRecord = 0;
  for(let imgId of imgList) {
    await Image.update(
     { _id: imgId}, { $set: { createUser: userId, createDate: Date.now() } }
    );
    updateRecord++;
  }
  return updateRecord;
};

var Image = mongoose.model('Image', ImageSchema);
module.exports = {Image};
