const express = require('express');
const app = express();
const server = require('http').createServer(app);  
const io = module.exports.io = require('socket.io')(server);
const axios = require("axios");
const cookieSession = require('cookie-session');
const passport = require('passport');
const keys = require('./config/keys');
require('./config/config');

const SocketManager = require('./socket/SocketManager');

const bodyParser = require('body-parser');

const PORT = process.env.PORT || 5000;
// configuration ===============================================================
const {mongoose} = require('./services/db/mongoose');
require('./services/passport');

app.use(
    cookieSession({
        maxAge: 2592000000, // 2592000000 = 30 * 24 * 60 * 60 * 1000
        keys: [keys.cookieKey]
    })
);
app.use(passport.initialize());
app.use(passport.session());

// create application/json parser
app.use(bodyParser.json());

// create application/x-www-form-urlencoded parser
app.use(bodyParser.urlencoded({ extended: true }));

// authenticate Routing
require('./routes/authRoutes')(app);

// API Routing
const routes = require('./routes/api');
app.use('/api', routes);

// Image Routing
const imgRouter = require('./routes/imageRoute');
app.use('/api', imgRouter);

app.get('*', function(req, res) {  
  res.sendFile(path.join( __dirname, '../src/index.html'));
});

if (process.env.NODE_ENV === 'production') {
  // Express will serve up production assets
  // like our main.js file, or main.css file!
  app.use(express.static('client/build'));

  // Express will serve up the index.html file
  // if it doesn't recognize the route
  const path = require('path');
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}

//io.on('connection', SocketManager);

server.listen(PORT, () => {
  console.log(`Started up at port ${PORT}`);
});

io.on('connection', (socket) => {  
	console.log('a user connected');

	socket.on('disconnect', () => {
		console.log('user disconnected');
	});

	socket.on('joinRoom', userId => {
		console.log(`a user joined ${userId}`);
		socket.join(userId);
	});

	socket.on('sendMessage', (message) => {
		sendMessage(socket, message);
	});
})

const sendMessage = async (socket, message) => {
	try {
		const res = await axios.post('http://localhost:5000/api/sendMessage', message);
		socket.broadcast.to(message.receiverId).emit('sendMessage', message);
		//io.sockets.in(message.receiverId).emit('sendMessage', message);
	} catch (error) {
		console.error(`Error: ${error.code}`);
	}
}