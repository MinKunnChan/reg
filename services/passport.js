const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const LocalStrategy   = require('passport-local').Strategy;
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const keys = require('../config/keys');
const {User} = require('../models/User');
const bcrypt = require('bcrypt-nodejs');
const { UserInfo }= require('../models/UserInfo');

passport.serializeUser((user, done) => {
  done(null, user.userId);
});

passport.deserializeUser((userId, done) => {
  User.findById(userId).then(user => {
      done(null, user);
    })
});

passport.use(
    new GoogleStrategy(
        {
            clientID: keys.googleClientID,
            clientSecret: keys.googleClientSecret,
            callbackURL: '/auth/google/callback',
            proxy: true
        },
        async (accessToken, refreshToken, profile, done) => {
            const existingUser = await User.findOne({ googleId: profile.id })

            if(existingUser) {
                const userObj = await User.getLoginUserInfo(existingUser._id);
                return done(null, userObj);
            }

            const user = await User.createUser({ googleId: profile.id},  profile.displayName);

            done(null, user);
        }
    )
);

passport.use(
    new FacebookStrategy(
        {
            clientID: keys.facebookClientID,
            clientSecret: keys.facebookClientSecret,
            callbackURL: '/auth/facebook/callback',
            profileFields: ['id', 'emails', 'displayName'],
            proxy: true
        },
        async (accessToken, refreshToken, profile, done) => {
            const existingUser = await User.findOne({ facebookId: profile.id });
            var email = profile.emails[0].value;

            if(existingUser) {
                const userObj = await User.getLoginUserInfo(existingUser._id);
                return done(null, userObj);
            }

            const user = await User.createUser({ facebookId: profile.id },  profile.displayName);
            done(null, user);
        }
    )
);

// Create signin local strategy
const localLogin = new LocalStrategy({ usernameField: 'email' }, function(email, password, done) {
  // Verify this email and password, call done with the user
  // if it is the correct email and password
  // otherwise, call done with false
  User.findOne({ email: email }, function(err, user) {
    if (err) { return done(err); }
    if (!user) { return done(null, false); }

    // compare passwords - is `password` equal to user.password?
    user.comparePassword(password, async function(err, isMatch) {
      if (err) { return done(err); }
      if (!isMatch) { return done(null, false); }
      const userObj = await User.getLoginUserInfo(user._id);
      return done(null, userObj);
    });
  });
});

// Create singup local strategy
const localSignup = new LocalStrategy({ 
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
    }, function(req, email, password, done) {
        var name = req.body.name;
        if (!email || !password || !name) {
            return res.status(422).send({ error: 'You must provide user name, email and password'});
        }
    
        // Check Email Exists
        User.findOne({ email: email }, async function(err, existingUser) {
            if (err) { return done(err); }
    
            // if a user with email exist, return an error
            if (existingUser) {
                return res.status(422).send({ error: 'Email is in use' });
            }
    
            // if email does not exist, create and save user
            const user = new User({
                email: email,
                password: password
            });
    
             // generate a salt then run callback
            await bcrypt.genSalt(10, function(err, salt) {
                if (err) { return done(err); }
    
                // hash (encrypt) our password using the salt
                bcrypt.hash(user.password, salt, null, function(err, hash) {
                if (err) { return done(err); }
    
                // overwrite plain text password with encrypted password
                user.password = hash;
                });
            });
    
            user.save(async function(err) {
                if (err) {
                    return done(err);
                }
    
                var userObj = {
                  userId: user._id,
                  name: name
                };
    
                userObj.profileImg = await UserInfo.createUserInfo(userObj);
    
                // Set default role as tourist.
                userObj.roleId = 1;
                return done(null, userObj);
                //res.json({ token : tokenForUser(user._id), user: userObj });
            });
        });
  });

// Setup options for JWT Strategy
const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: keys.secret
};

// Create JWT strategy
const jwtLogin = new JwtStrategy(jwtOptions, function(payload, done) {
    // See if the user ID in the payload exists in our database
    // If it does, call 'done' with that other
    // otherwise, call done without a user object
    User.findById(payload.sub, function(err, user) {
        if (err) { return done(err, false); }

        if (user) {
            done(null, user);
        } else {
            done(null, false);
        }
    });
});

// Tell passport to use this strategy
passport.use(jwtLogin);
passport.use('local-login', localLogin);
passport.use('local-signup', localSignup);
